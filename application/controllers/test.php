<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

	public function index()
	{
		$this->load->model('Package_model', 'Package');
		$this->load->model('Assembly_site_model', 'AssemblySite');
		$this->load->model('Product_line_model', 'ProductLine');
		$this->load->model('Category_model', 'Category');
		$this->load->model('Test_model', 'Test');
		$this->load->model('Condition_model', 'Condition');
		$this->load->model('Chamber_model', 'Chamber');

		$data = array();
		$data['packages'] = $this->Package->GetPackages();
		$data['assembly_sites'] = $this->AssemblySite->GetAssemblySites();
		$data['product_lines'] = $this->ProductLine->GetProductLines();
		$data['product_line_segments'] = $this->ProductLine->GetProductLineSegments();
		$data['categories'] = $this->Category->GetCategories();
		$data['tests'] = $this->Test->GetTests();
		$data['conditions'] = $this->Condition->GetConditions();
		$data['chambers'] = $this->Chamber->GetChambers();

		//$this->load->view('calendar', $data);
		$this->load->view('metis_template', $data);
	}

	public function reservation()
	{
		$this->load->model('package_model');
		$this->load->model('assembly_site_model');
		$this->load->model('product_line_model');
		$this->load->model('category_model');
		$this->load->model('test_model');
		$this->load->model('condition_model');
		$this->load->model('chamber_model');
		
		$data['sequence_no'] = md5(uniqid(rand(), true));
		//$data['sequence_no'] = md5(uniqid($your_user_login, true)); truly unique
		$data['packages'] = $this->package_model->GetPackages();
		$data['assembly_sites'] = $this->assembly_site_model->GetAssemblySites();
		$data['product_lines'] = $this->product_line_model->GetProductLines();
		$data['product_line_segments'] = $this->product_line_model->GetProductLineSegments();
		$data['categories'] = $this->category_model->GetCategories();
		$data['tests'] = $this->test_model->GetTests();
		$data['conditions'] = $this->condition_model->GetConditions();
		$data['chambers'] = $this->chamber_model->GetChambers();

		$this->load->view('reservation_form', $data);
	}

	public function reservation_list()
	{
		$this->load->view('reservation_list');
	}

	public function calendar_feed()
	{
		$this->load->model('Reservation_model', 'Reservation');

		$unix_start = $this->input->get('start');
		$unix_end = $this->input->get('end');

		$filters_array = array();

		/* Get filters */
		if($this->input->get('package')) 		$filters_array['package_id'] = $this->input->get('package');
		if($this->input->get('assembly_site')) 	$filters_array['assembly_site_id'] = $this->input->get('assembly_site');
		if($this->input->get('product_line')) 	$filters_array['product_line_segment_id'] = $this->input->get('product_line');
		if($this->input->get('category')) 		$filters_array['category_id'] = $this->input->get('category');
		if($this->input->get('test')) 			$filters_array['test_id'] = $this->input->get('test');
		if($this->input->get('condition')) 		$filters_array['condition_id'] = $this->input->get('condition');
		if($this->input->get('chamber')) 		$filters_array['chamber_id'] = $this->input->get('chamber');
		if(empty($filters_array)) 				$filters_array = false;

		/* Get Date range */
		$start_datetime = date('Y-m-d H:i:s', $unix_start);
		$end_datetime   = date('Y-m-d H:i:s', $unix_end);

		/* Pull reservations from database */
		$calendar_feed = $this->Reservation->GetReservationCalendar($start_datetime, $end_datetime, $filters_array);

		/* Process data to be displayed in calendar */
		$final_feed = array();
		foreach($calendar_feed as $reservation_array) {
			$reservation_array['start'] = $reservation_array['start_date'].' '.$reservation_array['start_time'];
			$reservation_array['end'] = $reservation_array['end_date'].' '.$reservation_array['end_time'];
			$reservation_array['title'] = $reservation_array['device'];
			$final_feed[] = $reservation_array;
		}

		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($final_feed));
	}
}