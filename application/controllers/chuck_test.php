<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chuck_test extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('metis_template');
	}

	function reservation_details($resevation_id = null)
	{
		$this->load->model('Reservation_model', 'Reservation');

		$data = array();
		$data['reservation_details'] = $this->Reservation->GetReservationDetails($resevation_id);		
		$this->load->view('reservation_details', $data);
	}

	
}