<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Chamber Calendar System</title>

	<link href="<?php echo base_url('css/fullcalendar.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('css/fullcalendar.print.css'); ?>" rel="stylesheet" media="print"/>	

	<link href="<?php echo base_url('css/jquery.qtip.css'); ?>" rel="stylesheet" />
	
	<link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet" />
</head>
<body>

	<div id='calendar'></div>

<div id="container">
	<h1>Welcome to the Test Page!</h1>
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

	<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.custom.min.js'); ?>"></script>
	<script src="<?php echo base_url('js/fullcalendar.js'); ?>"></script>

	<script>
		$(document).ready(function() {
			
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();			
			
			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				eventClick: function(data, event, view) {
					alert('Title: ' + data.title +
							'\nStart: ' + $.fullCalendar.formatDate(data.start,'yyyy-MM-dd H:mm') +
							'\nEnd: ' + $.fullCalendar.formatDate(data.end,'yyyy-MM-dd H:mm') + 
							'\nProduct: ' + data.product + 
							'\nSlots: ' + data.slots + 
							'\nEmployee: ' + data.employee
					);					
					if(data.url) {
						return false;
					}
				},

				editable: false,
				allDaySlot: false,
				slotMinutes: 60,
				allDayDefault: false,

				events: '<?php echo base_url("test/calendar_feed")?>',

			});						
		});	
		
	</script>
</body>
</html>