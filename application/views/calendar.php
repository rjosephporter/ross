<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">-->

    <title>Reliability Online Scheduling System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('lib/select2-3.4.5/select2.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/fullcalendar.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="main-content">

        <div class="filters-container panel panel-primary">
          <div class="panel-heading">
             <button type="button" class="btn btn-success" id="btn-show-filters">
              Filters <span class="caret"></span>
            </button>          
            <span class="label label-info">              
              Total Reservations
              <span class="badge">33</span>
            </span>            
            <span class="label label-info">              
              This Month
              <span class="badge">4</span>
            </span>
          </div>
          <div class="panel-body" id="filters-body" style="display:none">
            <div class="row">
              <div class="form-group col-md-3">
                <label for="package-filter" class="control-label">Package</label>
                <select multiple id="package-filter">
                  <?php foreach($packages as $package): ?>
                    <option value="<?php echo $package['package_id'] ?>"><?php echo $package['name'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="assembly-site-filter" class="control-label">Assembly Site</label>
                <select multiple id="assembly-site-filter">
                  <?php foreach($assembly_sites as $assembly_site): ?>
                    <option value="<?php echo $assembly_site['assembly_site_id'] ?>"><?php echo $assembly_site['name'] ?></option>
                  <?php endforeach; ?>                
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="product-line-filter" class="control-label">Product Line</label>
                <select multiple id="product-line-filter">
                  <?php foreach($product_lines as $product_line): ?>
                    <optgroup label="<?php echo $product_line['name'] ?>">
                    <?php foreach($product_line_segments as $product_line_segment): ?>
                      <?php if($product_line_segment['product_line_id'] === $product_line['product_line_id']): ?>
                        <option value="<?php echo $product_line_segment['product_line_segment_id'] ?>"><?php echo $product_line_segment['name'] ?></option>
                      <?php endif; ?>
                    <?php endforeach;?>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="category-filter" class="control-label">Category</label>
                <select multiple id="category-filter">
                  <?php foreach($categories as $category): ?>
                    <option value="<?php echo $category['category_id'] ?>"><?php echo $category['name'] ?></option>
                  <?php endforeach; ?>                
                </select>
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-3">
                <label for="test-filter" class="control-label">Test</label>
                <select multiple id="test-filter">
                  <?php foreach($tests as $test): ?>
                    <option value="<?php echo $test['test_id'] ?>"><?php echo $test['name'] ?></option>
                  <?php endforeach; ?>                
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="condition-filter" class="control-label">Condition</label>
                <select multiple id="condition-filter">
                  <?php foreach($conditions as $condition): ?>
                    <option value="<?php echo $condition['condition_id'] ?>"><?php echo $condition['name'] ?></option>
                  <?php endforeach; ?>                
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="chamber-filter" class="control-label">Chamber</label>
                <select multiple id="chamber-filter">
                  <?php foreach($chambers as $chamber): ?>
                    <option value="<?php echo $chamber['chamber_id'] ?>"><?php echo $chamber['name'] ?></option>
                  <?php endforeach; ?>                
                </select>
              </div>  
              <div class="form-group col-md-3">
                <div>
                  <button id="btn-apply-filter" type="submit" class="btn btn btn-primary">
                    Apply
                  </button>
                  <button id="btn-clear-filter" type="submit" class="btn btn btn-primary">
                    Clear
                  </button>
                  <!--
                  <button id="btn-add-reservation" type="submit" class="btn btn btn-primary">
                    <a href="#modal" class="second">Add Reservation</a>
                  </button>
                  -->
                </div>
              </div>      
            </div>
          </div>
        </div>


        </div>

        <div id='calendar'></div>

        <div id="container">
          <h1>Welcome to the Test Page!</h1>
          <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>  
      </div>      

    </div><!-- /.container -->

    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Reservation Information</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">User</label>
                <div class="col-sm-8">
                  <p id="rd-employee" class="form-control-static">John Doe</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Sequence Number</label>
                <div class="col-sm-8">
                  <p id="rd-sequence-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Qual Lot Number</label>
                <div class="col-sm-8">
                  <p id="rd-qual-lot-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Assembly Lot Number</label>
                <div class="col-sm-8">
                  <p id="rd-assembly-lot-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Rel Number</label>
                <div class="col-sm-8">
                  <p id="rd-rel-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Package</label>
                <div class="col-sm-8">
                  <p id="rd-package" class="form-control-static">SOT23</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Device</label>
                <div class="col-sm-8">
                  <p id="rd-device" class="form-control-static">Samsung Galaxy</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Datasheet</label>
                <div class="col-sm-8">
                  <p id="rd-datasheet" class="form-control-static">//ngee/ngoo/boom.doc</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Qual Plan</label>
                <div class="col-sm-8">
                  <p id="rd-qual-plan" class="form-control-static">//ngee/ngoo/boom.pdf</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Purpose</label>
                <div class="col-sm-8">
                  <p id="rd-purpose" class="form-control-static">This is the not so lengthy purpose of the test.</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Background</label>
                <div class="col-sm-8">
                  <p id="rd-background" class="form-control-static">This is the very lengthy background of the test.</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Assembly Site</label>
                <div class="col-sm-8">
                  <p id="rd-assembly-site" class="form-control-static">FSCP</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Product Line</label>
                <div class="col-sm-8">
                  <p id="rd-product-line" class="form-control-static">MCCC</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Product Line Segment</label>
                <div class="col-sm-8">
                  <p id="rd-product-line-segment" class="form-control-static">Mid Power Analog</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Number of Dice</label>
                <div class="col-sm-8">
                  <p id="rd-number-of-dice" class="form-control-static">5</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Category</label>
                <div class="col-sm-8">
                  <p id="rd-category" class="form-control-static">Assembly/Subcon</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Test</label>
                <div class="col-sm-8">
                  <p id="rd-test" class="form-control-static">HTRB</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Condition</label>
                <div class="col-sm-8">
                  <p id="rd-condition" class="form-control-static">125°C / VCC=?</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Chamber</label>
                <div class="col-sm-8">
                  <p id="rd-chamber" class="form-control-static">#16</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Number of Slots</label>
                <div class="col-sm-8">
                  <p id="rd-number-of-storage" class="form-control-static">10</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Start Date</label>
                <div class="col-sm-8">
                  <p id="rd-start" class="form-control-static">2013-11-25 04:00:00</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">End Date</label>
                <div class="col-sm-8">
                  <p id="rd-end" class="form-control-static">2013-11-30 08:00:00</p>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/jquery-ui-1.10.3.custom.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
  
    <script src="<?php echo base_url('lib/select2-3.4.5/select2.js'); ?>"></script>
    <script src="<?php echo base_url('js/fullcalendar.js'); ?>"></script>

    <script>
    $(document).ready(function() {      

      var event_source = 
            {
              url: '<?php echo base_url("test/calendar_feed")?>',
              type: 'GET'
            }
          ;
         
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        eventClick: function(data, event, view) {
          /* Populate reservation details */
          $('#rd-employee').text(data.employee);
          $('#rd-sequence-number').text(data.sequence_number);
          $('#rd-qual-lot-number').text(data.qual_lot_number);
          $('#rd-assembly-lot-number').text(data.assembly_lot_number);
          $('#rd-rel-number').text(data.rel_number);
          $('#rd-package').text(data.package_name);
          $('#rd-device').text(data.device);
          $('#rd-datasheet').text(data.datasheet_path);
          $('#rd-qual-plan').text(data.qual_plan_path);
          $('#rd-purpose').text(data.purpose);
          $('#rd-background').text(data.background);
          $('#rd-assembly_site').text(data.assembly_site_name);
          $('#rd-product-line').text(data.product_line_name);
          $('#rd-product-line-segment').text(data.product_line_segment_name);
          $('#rd-number-of-dice').text(data.number_of_dice);
          $('#rd-category').text(data.category_name);
          $('#rd-test').text(data.test_name);
          $('#rd-condition').text(data.condition_name);
          $('#rd-chamber').text(data.chamber_name);
          $('#rd-number-of-storage').text(data.number_of_storage);
          $('#rd-start').text(data.start);
          $('#rd-end').text(data.end);

          /* Show reservation details */
          $('#myModal').modal();
        },

        windowResize: function(event, ui) {
            $('#calendar').fullCalendar('render');
        },

        editable: false,
        allDaySlot: false,
        slotMinutes: 60,
        allDayDefault: false,

        eventSources: [ event_source ]

      }); 

      $('#btn-show-filters').click(function(){
        $('#filters-body').toggle();
      });

      $('select').select2({
          placeholder: "Select",
          allowClear: true
      });

      $('#btn-clear-filter').click(function(){
        $('select').select2('val','');
      });

      $('#btn-apply-filter').click(function(){
          
          $('#calendar').fullCalendar('removeEventSource',event_source);          
          $('#calendar').fullCalendar('removeEvents');
          
          var source =
            {
              url: '<?php echo base_url("test/calendar_feed")?>',
              type: 'GET',
              data: {
                package: $('#package-filter').select2('val').join(),
                assembly_site: $('#assembly-site-filter').select2('val').join(),
                product_line: $('#product-line-filter').select2('val').join(),
                category: $('#category-filter').select2('val').join(),
                test: $('#test-filter').select2('val').join(),
                condition: $('#condition-filter').select2('val').join(),
                chamber: $('#chamber-filter').select2('val').join(),
              }
            };

          $('#calendar').fullCalendar('addEventSource', source);
          $('#calendar').fullCalendar('rerenderEvents');
          event_source = source;
      });          

    }); 
    
  </script>
  
  </body>
</html>
