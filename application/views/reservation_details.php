<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">-->

    <title>Reliability Online Scheduling System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('lib/select2-3.4.5/select2.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="main-content">
        <div class="row">

          <div class="col-md-7">
            <div class="panel panel-primary">
              <div class="panel-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Package</label>
                    <div class="col-sm-9">
                      <p class="form-control-static"><?php echo $reservation_details['package_name'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Device</label>
                    <div class="col-sm-9">
                      <p class="form-control-static"><?php echo $reservation_details['device'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Datasheet</label>
                    <div class="col-sm-9">
                      <p class="form-control-static"><?php echo $reservation_details['datasheet_path'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Qual Plan</label>
                    <div class="col-sm-9">
                      <p class="form-control-static"><?php echo $reservation_details['qual_plan_path'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Assembly Site</label>
                    <div class="col-sm-9">
                      <p class="form-control-static"><?php echo $reservation_details['assembly_site_name'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Product Line</label>
                    <div class="col-sm-3">
                      <p class="form-control-static"><?php echo $reservation_details['product_line_name'] ?></p>
                    </div>
                    <label class="col-sm-3 control-label">Segment</label>
                    <div class="col-sm-3">
                      <p class="form-control-static"><?php echo $reservation_details['product_line_segment_name'] ?></p>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="col-md-5">
            <div class="panel panel-primary">
              <div class="panel-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Reservation No.</label>
                    <div class="col-sm-8">
                      <p class="form-control-static"><?php echo $reservation_details['reservation_id'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Sequence No.</label>
                    <div class="col-sm-8">
                      <p class="form-control-static"><?php echo $reservation_details['sequence_number'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Qual Lot No.</label>
                    <div class="col-sm-8">
                      <p class="form-control-static"><?php echo $reservation_details['qual_lot_number'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Assembly Lot No.</label>
                    <div class="col-sm-8">
                      <p class="form-control-static"><?php echo $reservation_details['assembly_lot_number'] ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Rel No.</label>
                    <div class="col-sm-8">
                      <p class="form-control-static"><?php echo $reservation_details['rel_number'] ?></p>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-primary">
              <div class="panel-body">

                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading"><label>Purpose</label></div>
                    <div class="panel-body">
                      <?php echo $reservation_details['purpose'] ?>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading"><label>Background</label></div>
                    <div class="panel-body">
                      <?php echo $reservation_details['background'] ?>
                    </div>
                  </div>
                </div>               

                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <form class="form-horizontal">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Category</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['category_name'] ?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Test</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['test_name'] ?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Condition</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['condition_name'] ?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Chamber</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['chamber_name'] ?></p>
                          </div>
                        </div>                        
                      </form>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <form class="form-horizontal">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Number of Dice</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['number_of_dice'] ?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Number of Slots</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['number_of_storage'] ?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Start Date</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['start_date'] ?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label">End Date</label>
                          <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $reservation_details['end_date'] ?></p>
                          </div>
                        </div>                        
                      </form>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <table class="table table-condensed">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Fab Site</th>
                        <th>Die Technology</th>
                        <th>Die Run</th>
                        <th>Wafer No.</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>FSSL</td>
                        <td>Option 1</td>
                        <td>123001</td>
                        <td>700001</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>FSME</td>
                        <td>Option 2</td>
                        <td>123002</td>
                        <td>700002</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>TSMC</td>
                        <td>Option 3</td>
                        <td>123003</td>
                        <td>700003</td>
                      </tr>
                    </tbody>
                  </table>
                </div>

              </div>                
            </div>
          </div>
        </div>

      </div>
      <div id="container">
        <h1>Welcome to the Test Page!</h1>
        <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
      </div>  
    </div>      

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/jquery-ui-1.10.3.custom.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
  
    <script src="<?php echo base_url('lib/select2-3.4.5/select2.js'); ?>"></script>
  
  </body>
</html>
