<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Reliability Online Scheduling System</title>

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Bootstrap -->
    <link href="<?php echo base_url('css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo base_url('lib/font-awesome-4.0.3/css/font-awesome.css'); ?>" rel="stylesheet">

    <!-- Select2 -->
    <link href="<?php echo base_url('lib/select2-3.4.5/select2.css'); ?>" rel="stylesheet">

    <!-- FullCalendar -->
    <link href="<?php echo base_url('css/fullcalendar.css'); ?>" rel="stylesheet">

    <!-- Metis core stylesheet -->
    <link href="<?php echo base_url('css/main.css'); ?>" rel="stylesheet">

    <!-- R.O.S.S stylesheet -->
    <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/lib/html5shiv/html5shiv.js"></script>
	      <script src="assets/lib/respond/respond.min.js"></script>
	    <![endif]-->

    <!--Modernizr-->
    <script src="<?php echo base_url('js/modernizr.custom.05129.js'); ?>"></script>
  </head>
  <body>
    <div id="wrap">
      <div id="top">

        <!-- .navbar -->
        <nav class="navbar navbar-inverse navbar-static-top">

          <!-- Brand and toggle get grouped for better mobile display -->
          <header class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="index.html" class="navbar-brand">
              <img src="<?php echo base_url('images/logo.png') ?>" alt="">
            </a>
          </header>
          <div class="topnav">
            <div class="btn-toolbar">
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="Show / Hide Sidebar" data-toggle="tooltip" class="btn btn-success btn-sm" id="changeSidebarPos">
                  <i class="fa fa-expand"></i>
                </a>
              </div>
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="E-mail" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-envelope"></i>
                  <span class="label label-warning">5</span>
                </a>
                <a data-placement="bottom" data-original-title="Messages" href="#" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-comments"></i>
                  <span class="label label-danger">4</span>
                </a>
              </div>
              <div class="btn-group">
                <a data-placement="bottom" data-original-title="Document" href="#" data-toggle="tooltip" class="btn btn-default btn-sm">
                  <i class="fa fa-file"></i>
                </a>
                <a data-toggle="modal" data-original-title="Help" data-placement="bottom" class="btn btn-default btn-sm" href="#helpModal">
                  <i class="fa fa-question"></i>
                </a>
              </div>
              <div class="btn-group">
                <a href="login.html" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom" class="btn btn-metis-1 btn-sm">
                  <i class="fa fa-power-off"></i>
                </a>
              </div>
            </div>
          </div><!-- /.topnav -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">

            <!-- .nav -->
            <ul class="nav navbar-nav">
              <li> <a href="dashboard.html">Dashboard</a> </li>
              <li> <a href="table.html">Tables</a> </li>
              <li> <a href="file.html">File Manager</a> </li>
              <li class='dropdown '>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Form Elements
                  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li> <a href="form-general.html">General</a> </li>
                  <li> <a href="form-validation.html">Validation</a> </li>
                  <li> <a href="form-wysiwyg.html">WYSIWYG</a> </li>
                  <li> <a href="form-wizard.html">Wizard &amp; File Upload</a> </li>
                </ul>
              </li>
            </ul><!-- /.nav -->
          </div>
        </nav><!-- /.navbar -->
      </div><!-- /#top -->
      <div id="left">
        <div class="media user-media">
          <a class="user-link" href="">
            <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif">
            <span class="label label-danger user-label">16</span>
          </a>
          <div class="media-body">
            <h5 class="media-heading">Archie</h5>
            <ul class="list-unstyled user-info">
              <li> <a href="">Administrator</a> </li>
              <li>Last Access :
                <br>
                <small>
                  <i class="fa fa-calendar"></i>&nbsp;16 Mar 16:32</small>
              </li>
            </ul>
          </div>
        </div>

        <!-- #menu -->
        <ul id="menu" class="collapse">
          <li class="nav-divider"></li>
          <li class="">
            <a href="javascript:;">
              <i class="fa fa-dashboard"></i>
              <span class="link-title">Dashboard</span>
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li class="">
                <a href="dashboard.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Default Style
                </a>
              </li>
              <li class="">
                <a href="alterne.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Alternative Style
                </a>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>&nbsp;Components
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li class="">
                <a href="icon.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Icon</a>
              </li>
              <li class="">
                <a href="button.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Button</a>
              </li>
              <li class="">
                <a href="progress.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Progress</a>
              </li>
              <li class="">
                <a href="pricing.html">
                  <i class="fa fa-credit-card"></i>&nbsp;Pricing Table</a>
              </li>
              <li class="">
                <a href="bgimage.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Bg Image</a>
              </li>
              <li class="">
                <a href="bgcolor.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Bg Color</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="javascript:;">
              <i class="fa fa-pencil"></i>&nbsp;Forms
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li class="">
                <a href="form-general.html">
                  <i class="fa fa-angle-right"></i>&nbsp;General</a>
              </li>
              <li class="">
                <a href="form-validation.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Validation</a>
              </li>
              <li class="">
                <a href="form-wysiwyg.html">
                  <i class="fa fa-angle-right"></i>&nbsp;WYSIWYG</a>
              </li>
              <li class="">
                <a href="form-wizard.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Wizard &amp; File Upload</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="table.html">
              <i class="fa fa-table"></i>&nbsp; Tables</a>
          </li>
          <li>
            <a href="file.html">
              <i class="fa fa-file"></i>&nbsp;File Manager</a>
          </li>
          <li>
            <a href="typography.html">
              <i class="fa fa-font"></i>&nbsp; Typography</a>
          </li>
          <li>
            <a href="maps.html">
              <i class="fa fa-map-marker"></i>&nbsp;Maps</a>
          </li>
          <li>
            <a href="chart.html">
              <i class="fa fa fa-bar-chart-o"></i>&nbsp;Charts</a>
          </li>
          <li>
            <a href="calendar.html">
              <i class="fa fa-calendar"></i>&nbsp;Calendar</a>
          </li>
          <li>
            <a href="javascript:;">
              <i class="fa fa-exclamation-triangle"></i>&nbsp;Error Pages
              <span class="fa arrow"></span>
            </a>
            <ul>
              <li>
                <a href="403.html">
                  <i class="fa fa-angle-right"></i>&nbsp;403</a>
              </li>
              <li>
                <a href="404.html">
                  <i class="fa fa-angle-right"></i>&nbsp;404</a>
              </li>
              <li>
                <a href="405.html">
                  <i class="fa fa-angle-right"></i>&nbsp;405</a>
              </li>
              <li>
                <a href="500.html">
                  <i class="fa fa-angle-right"></i>&nbsp;500</a>
              </li>
              <li>
                <a href="503.html">
                  <i class="fa fa-angle-right"></i>&nbsp;503</a>
              </li>
              <li>
                <a href="offline.html">
                  <i class="fa fa-angle-right"></i>&nbsp;offline</a>
              </li>
              <li>
                <a href="countdown.html">
                  <i class="fa fa-angle-right"></i>&nbsp;Under Construction</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="grid.html">
              <i class="fa fa-columns"></i>&nbsp;Grid</a>
          </li>
          <li class="active">
            <a href="blank.html">
              <i class="fa fa-square-o"></i>&nbsp;Blank Page</a>
          </li>
          <li class="nav-divider"></li>
          <li>
            <a href="login.html">
              <i class="fa fa-sign-in"></i>&nbsp;Login Page</a>
          </li>
          <li>
            <a href="javascript:;">Unlimited Level Menu  <span class="fa arrow"></span> </a>
            <ul>
              <li>
                <a href="javascript:;">Level 1  <span class="fa arrow"></span> </a>
                <ul>
                  <li> <a href="javascript:;">Level 2</a> </li>
                  <li> <a href="javascript:;">Level 2</a> </li>
                  <li>
                    <a href="javascript:;">Level 2  <span class="fa arrow"></span> </a>
                    <ul>
                      <li> <a href="javascript:;">Level 3</a> </li>
                      <li> <a href="javascript:;">Level 3</a> </li>
                      <li>
                        <a href="javascript:;">Level 3  <span class="fa arrow"></span> </a>
                        <ul>
                          <li> <a href="javascript:;">Level 4</a> </li>
                          <li> <a href="javascript:;">Level 4</a> </li>
                          <li>
                            <a href="javascript:;">Level 4  <span class="fa arrow"></span> </a>
                            <ul>
                              <li> <a href="javascript:;">Level 5</a> </li>
                              <li> <a href="javascript:;">Level 5</a> </li>
                              <li> <a href="javascript:;">Level 5</a> </li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                      <li> <a href="javascript:;">Level 4</a> </li>
                    </ul>
                  </li>
                  <li> <a href="javascript:;">Level 2</a> </li>
                </ul>
              </li>
              <li> <a href="javascript:;">Level 1</a> </li>
              <li>
                <a href="javascript:;">Level 1  <span class="fa arrow"></span> </a>
                <ul>
                  <li> <a href="javascript:;">Level 2</a> </li>
                  <li> <a href="javascript:;">Level 2</a> </li>
                  <li> <a href="javascript:;">Level 2</a> </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul><!-- /#menu -->
      </div><!-- /#left -->
      <div id="content">
        <!-- .outer -->
        <div class="outer">

          <!-- .inner -->
          <div class="inner">

            <div class="filters-container box success">
              <header>
                <div class="icons">
                  <i class="fa fa-filter"></i>
                </div>
                <h5>Filters</h5>
                <div class="toolbar">
                  <button class="btn btn-primary btn-sm btn-round" data-toggle="collapse" data-target="#filters-body">round</button>
                </div>
              </header>
              <div class="body collapse" id="filters-body">
                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="package-filter" class="control-label">Package</label>
                    <select multiple id="package-filter">
                      <?php foreach($packages as $package): ?>
                        <option value="<?php echo $package['package_id'] ?>"><?php echo $package['name'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="assembly-site-filter" class="control-label">Assembly Site</label>
                    <select multiple id="assembly-site-filter">
                      <?php foreach($assembly_sites as $assembly_site): ?>
                        <option value="<?php echo $assembly_site['assembly_site_id'] ?>"><?php echo $assembly_site['name'] ?></option>
                      <?php endforeach; ?>                
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="product-line-filter" class="control-label">Product Line</label>
                    <select multiple id="product-line-filter">
                      <?php foreach($product_lines as $product_line): ?>
                        <optgroup label="<?php echo $product_line['name'] ?>">
                        <?php foreach($product_line_segments as $product_line_segment): ?>
                          <?php if($product_line_segment['product_line_id'] === $product_line['product_line_id']): ?>
                            <option value="<?php echo $product_line_segment['product_line_segment_id'] ?>"><?php echo $product_line_segment['name'] ?></option>
                          <?php endif; ?>
                        <?php endforeach;?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="category-filter" class="control-label">Category</label>
                    <select multiple id="category-filter">
                      <?php foreach($categories as $category): ?>
                        <option value="<?php echo $category['category_id'] ?>"><?php echo $category['name'] ?></option>
                      <?php endforeach; ?>                
                    </select>
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="test-filter" class="control-label">Test</label>
                    <select multiple id="test-filter">
                      <?php foreach($tests as $test): ?>
                        <option value="<?php echo $test['test_id'] ?>"><?php echo $test['name'] ?></option>
                      <?php endforeach; ?>                
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="condition-filter" class="control-label">Condition</label>
                    <select multiple id="condition-filter">
                      <?php foreach($conditions as $condition): ?>
                        <option value="<?php echo $condition['condition_id'] ?>"><?php echo $condition['name'] ?></option>
                      <?php endforeach; ?>                
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="chamber-filter" class="control-label">Chamber</label>
                    <select multiple id="chamber-filter">
                      <?php foreach($chambers as $chamber): ?>
                        <option value="<?php echo $chamber['chamber_id'] ?>"><?php echo $chamber['name'] ?></option>
                      <?php endforeach; ?>                
                    </select>
                  </div>  
                  <div class="form-group col-md-3">
                    <div>
                      <button id="btn-apply-filter" type="submit" class="btn btn btn-primary">
                        Apply
                      </button>
                      <button id="btn-clear-filter" type="submit" class="btn btn btn-primary">
                        Clear
                      </button>
                      <!--
                      <button id="btn-add-reservation" type="submit" class="btn btn btn-primary">
                        <a href="#modal" class="second">Add Reservation</a>
                      </button>
                      -->
                    </div>
                  </div>      
                </div>
              </div><!-- /.body -->
            </div>

            <!-- FullCalendar -->
            <div id='calendar'></div>

          </div>
          <!-- end .inner -->

        </div>
        <!-- end .outer -->
      </div>

      <!-- end #content -->
    </div><!-- /#wrap -->
    <div id="footer">
      <p>2013 &copy; Metis Admin</p>
    </div>

    <!-- #helpModal -->
    <div id="helpModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --><!-- /#helpModal -->

    <!-- #myModal -->
    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Reservation Information</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">User</label>
                <div class="col-sm-8">
                  <p id="rd-employee" class="form-control-static">John Doe</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Sequence Number</label>
                <div class="col-sm-8">
                  <p id="rd-sequence-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Qual Lot Number</label>
                <div class="col-sm-8">
                  <p id="rd-qual-lot-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Assembly Lot Number</label>
                <div class="col-sm-8">
                  <p id="rd-assembly-lot-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Rel Number</label>
                <div class="col-sm-8">
                  <p id="rd-rel-number" class="form-control-static">123700</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Package</label>
                <div class="col-sm-8">
                  <p id="rd-package" class="form-control-static">SOT23</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Device</label>
                <div class="col-sm-8">
                  <p id="rd-device" class="form-control-static">Samsung Galaxy</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Datasheet</label>
                <div class="col-sm-8">
                  <p id="rd-datasheet" class="form-control-static">//ngee/ngoo/boom.doc</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Qual Plan</label>
                <div class="col-sm-8">
                  <p id="rd-qual-plan" class="form-control-static">//ngee/ngoo/boom.pdf</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Purpose</label>
                <div class="col-sm-8">
                  <p id="rd-purpose" class="form-control-static">This is the not so lengthy purpose of the test.</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Background</label>
                <div class="col-sm-8">
                  <p id="rd-background" class="form-control-static">This is the very lengthy background of the test.</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Assembly Site</label>
                <div class="col-sm-8">
                  <p id="rd-assembly-site" class="form-control-static">FSCP</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Product Line</label>
                <div class="col-sm-8">
                  <p id="rd-product-line" class="form-control-static">MCCC</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Product Line Segment</label>
                <div class="col-sm-8">
                  <p id="rd-product-line-segment" class="form-control-static">Mid Power Analog</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Number of Dice</label>
                <div class="col-sm-8">
                  <p id="rd-number-of-dice" class="form-control-static">5</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Category</label>
                <div class="col-sm-8">
                  <p id="rd-category" class="form-control-static">Assembly/Subcon</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Test</label>
                <div class="col-sm-8">
                  <p id="rd-test" class="form-control-static">HTRB</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Condition</label>
                <div class="col-sm-8">
                  <p id="rd-condition" class="form-control-static">125°C / VCC=?</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Chamber</label>
                <div class="col-sm-8">
                  <p id="rd-chamber" class="form-control-static">#16</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Number of Slots</label>
                <div class="col-sm-8">
                  <p id="rd-number-of-storage" class="form-control-static">10</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Start Date</label>
                <div class="col-sm-8">
                  <p id="rd-start" class="form-control-static">2013-11-25 04:00:00</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">End Date</label>
                <div class="col-sm-8">
                  <p id="rd-end" class="form-control-static">2013-11-30 08:00:00</p>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <!-- end #myModal -->

    <!-- Javascripts -->
    <script src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('lib/select2-3.4.5/select2.js'); ?>"></script>
    <script src="<?php echo base_url('js/fullcalendar.js'); ?>"></script>
    <script src="<?php echo base_url('js/main.js'); ?>"></script>

    <script>
      $(document).ready(function() {      

        var event_source = 
              {
                url: '<?php echo base_url("test/calendar_feed")?>',
                type: 'GET'
              }
            ;
           
        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          eventClick: function(data, event, view) {
            /* Populate reservation details */
            $('#rd-employee').text(data.employee);
            $('#rd-sequence-number').text(data.sequence_number);
            $('#rd-qual-lot-number').text(data.qual_lot_number);
            $('#rd-assembly-lot-number').text(data.assembly_lot_number);
            $('#rd-rel-number').text(data.rel_number);
            $('#rd-package').text(data.package_name);
            $('#rd-device').text(data.device);
            $('#rd-datasheet').text(data.datasheet_path);
            $('#rd-qual-plan').text(data.qual_plan_path);
            $('#rd-purpose').text(data.purpose);
            $('#rd-background').text(data.background);
            $('#rd-assembly_site').text(data.assembly_site_name);
            $('#rd-product-line').text(data.product_line_name);
            $('#rd-product-line-segment').text(data.product_line_segment_name);
            $('#rd-number-of-dice').text(data.number_of_dice);
            $('#rd-category').text(data.category_name);
            $('#rd-test').text(data.test_name);
            $('#rd-condition').text(data.condition_name);
            $('#rd-chamber').text(data.chamber_name);
            $('#rd-number-of-storage').text(data.number_of_storage);
            $('#rd-start').text(data.start);
            $('#rd-end').text(data.end);

            /* Show reservation details */
            $('#myModal').modal();
          },

          windowResize: function(event, ui) {
              $('#calendar').fullCalendar('render');
          },

          eventRender: function(event, element) {
            element.popover({
              content: 'NGEEE!',
              placement: 'bottom',
              trigger: 'hover'
            });
          },

          editable: false,
          allDaySlot: false,
          slotMinutes: 60,
          allDayDefault: false,

          eventSources: [ event_source ]

        }); 

        $('select').select2({
            placeholder: "Select",
            allowClear: true
        });

        $('#btn-clear-filter').click(function(){
          $('select').select2('val','');
        });

        $('#btn-apply-filter').click(function(){
            
            $('#calendar').fullCalendar('removeEventSource',event_source);          
            $('#calendar').fullCalendar('removeEvents');
            
            var source =
              {
                url: '<?php echo base_url("test/calendar_feed")?>',
                type: 'GET',
                data: {
                  package: $('#package-filter').select2('val').join(),
                  assembly_site: $('#assembly-site-filter').select2('val').join(),
                  product_line: $('#product-line-filter').select2('val').join(),
                  category: $('#category-filter').select2('val').join(),
                  test: $('#test-filter').select2('val').join(),
                  condition: $('#condition-filter').select2('val').join(),
                  chamber: $('#chamber-filter').select2('val').join(),
                }
              };

            $('#calendar').fullCalendar('addEventSource', source);
            $('#calendar').fullCalendar('rerenderEvents');
            event_source = source;
        });          

      }); 
    
    </script>    

  </body>
</html>