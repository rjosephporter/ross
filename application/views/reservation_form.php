<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">-->

    <title>Reliability Online Scheduling System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->    
    <link href="<?php echo base_url('lib/select2-3.4.5/select2.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('lib/bootstrap-daterangepicker/daterangepicker-bs3.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

      <div class="main-content"> 
        <form class="form-horizontal" role="form">

          <!-- first group -->
          <div class="set-group">
            <div class="form-group">
              <label for="inputSequenceNo" class="col-sm-2 control-label">Sequence No.</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputSequenceNo" readonly="readonly" value="<?php echo $sequence_no; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="inputDateRange" class="col-sm-2 control-label">Date Range</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputDateRange" placeholder="Date Range" readonly="readonly" required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputQualLotNo" class="col-sm-2 control-label">Qual Lot No.</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputQualLotNo" placeholder="Qual Lot No." required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputRelNo" class="col-sm-2 control-label">Rel No.</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputRelNo" placeholder="Rel No." required>
              </div>
            </div>
            <div class="form-group">
              <label for="selectPackage" class="col-sm-2 control-label">Package</label>
              <div class="col-sm-10">
                <select id="selectPackage" required>
                  <option></option>
                  <?php foreach($packages as $package): ?>
                    <option value="<?php echo $package['package_id']; ?>"><?php echo $package['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputDevice" class="col-sm-2 control-label">Device</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputDevice" placeholder="Device">
              </div>
            </div>
            <div class="form-group">
              <label for="inputDatasheet" class="col-sm-2 control-label">Datasheet</label>
              <div class="col-sm-10">
                <input type="file" class="form-control" id="inputDatasheet" placeholder="Browse.." required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputQualPlan" class="col-sm-2 control-label">Qual Plan</label>
              <div class="col-sm-10">
                <input type="file" class="form-control" id="inputQualPlan" placeholder="Browse.." required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPurpose" class="col-sm-2 control-label">Purpose</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputPurpose" placeholder="Purpose">
              </div>
            </div>
            <div class="form-group">
              <label for="inputBackground" class="col-sm-2 control-label">Background</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="inputBackground" placeholder="Background"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="selectAssemblySite" class="col-sm-2 control-label">Assembly Site</label>
              <div class="col-sm-10">
                <select id="selectAssemblySite" required>
                  <option></option>
                  <?php foreach($assembly_sites as $assembly_site): ?>
                    <option value="<?php echo $assembly_site['assembly_site_id']; ?>"><?php echo $assembly_site['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="selectProductLine" class="col-sm-2 control-label">Product Line</label>
              <div class="col-sm-10">
                <select id="selectProductLine" required>
                  <option></option>
                  <?php foreach($product_lines as $product_line): ?>
                    <option value="<?php echo $product_line['product_line_id']; ?>"><?php echo $product_line['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="selectProductLineSegment" class="col-sm-2 control-label">Product Line Segment</label>
              <div class="col-sm-10">
                <select id="selectProductLineSegment" required>
                  <option></option>
                  <?php foreach($product_line_segments as $product_line_segment): ?>
                    <option value="<?php echo $product_line_segment['product_line_id']; ?>"><?php echo $product_line['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <a class="btn btn-success next">Next</a>
              </div>
            </div>
          </div>

          <!-- second group -->
          <div class="set-group">
            <div class="form-group">
              <label for="inputNoOfDice" class="col-sm-2 control-label">No. of Dice</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" id="inputNoOfDice" min=1 max=7 step=1>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2 bold right">
                Fab Site 1
              </div>
              <div class="col-sm-10">
                <table style="width:100%">
                  <tr>
                    <td class="bold dice-label">
                      <label for="selectFabSite1" class="control-label">Fab Site 1</label>
                    </td>
                    <td>
                      <select id="selectCategory">
                        <option></option>
                        <option value="A">Chamber 1 - This is Chamber 1</option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                  </tr>
                </table>
              </div>
            </div>
            <div class="form-group">
              <label for="selectCategory" class="col-sm-2 control-label">Category</label>
              <div class="col-sm-10">
                <select id="selectCategory">
                  <option></option>
                  <option value="A">Chamber 1 - This is Chamber 1</option>
                  <option value="B">Chamber 2 - This is Chamber 2</option>
                  <option value="C">Chamber 3 - This is Chamber 3</option>
                  <option value="D">Chamber 4 - This is Chamber 4</option>
                  <option value="E">Chamber 5 - This is Chamber 5</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="selectSlot" class="col-sm-2 control-label">Slot</label>
              <div class="col-sm-10">
                <select multiple id="selectSlot">
                  <option value="1">Slot 1</option>
                  <option value="2">Slot 2</option>
                  <option value="3">Slot 3</option>
                  <option value="4">Slot 4</option>
                  <option value="5">Slot 5</option>
                  <option value="6">Slot 6</option>
                  <option value="7">Slot 7</option>
                  <option value="8">Slot 8</option>
                  <option value="9">Slot 9</option>
                  <option value="10">Slot 10</option>
                  <option value="11">Slot 11</option>
                  <option value="12">Slot 12</option>
                  <option value="13">Slot 13</option>
                  <option value="14">Slot 14</option>
                  <option value="15">Slot 15</option>
                  <option value="16">Slot 16</option>
                  <option value="17">Slot 17</option>
                  <option value="18">Slot 18</option>
                  <option value="19">Slot 19</option>
                  <option value="20">Slot 20</option>
                </select>
              </div>
            </div>
  
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <a class="btn btn-success prev">Previous</a>
                <a class="btn btn-success next">Next</a>
              </div>
            </div>
          </div>

          <!-- third group -->
          <div class="set-group">
            <div class="form-group">
              <label for="selectCategory" class="col-sm-2 control-label">Category</label>
              <div class="col-sm-10">
                <select id="selectCategory">
                  <option></option>
                  <?php foreach($categories as $category): ?>
                    <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="selectTest" class="col-sm-2 control-label">Test</label>
              <div class="col-sm-10">
                <select id="selectTest">
                  <option></option>
                  <?php foreach($tests as $test): ?>
                    <option value="<?php echo $test['test_id']; ?>"><?php echo $test['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="selectCondition" class="col-sm-2 control-label">Condition</label>
              <div class="col-sm-10">
                <select id="selectCondition">
                  <option></option>
                  <?php foreach($conditions as $condition): ?>
                    <option value="<?php echo $condition['condition_id']; ?>"><?php echo $condition['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="selectChambers" class="col-sm-2 control-label">Chambers</label>
              <div class="col-sm-10">
                <select id="selectChambers">
                  <option></option>
                  <?php foreach($conditions as $condition): ?>
                    <option value="<?php echo $condition['condition_id']; ?>"><?php echo $condition['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
  
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <a class="btn btn-success prev">Previous</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>

      <div id="container">
        <h1>Welcome to the Test Page!</h1>
        <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
      </div>      

    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>-->
    <script src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
    <!--<script src="<?php echo base_url('js/jquery-ui.custom.min.js'); ?>"></script>-->
    <script src="<?php echo base_url('js/jquery-ui-1.10.3.custom.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>

    <!--<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>-->
    
    <script src="<?php echo base_url('lib/select2-3.4.5/select2.js'); ?>"></script>
    <script src="<?php echo base_url('lib/bootstrap-daterangepicker/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('lib/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

    <script>
      $(document).ready(function() {
        $("select").select2({
          placeholder: "SELECT",
          allowClear: true
        });
        $('#inputDateRange').daterangepicker({
          timePicker: true,
          timePickerIncrement: 60,
          format: 'MM/DD/YYYY h:mm A'
        },
        function(start,end) {
          alert(start+' --- '+end);
        });

        $(".next").click(function()
        {
          var next = $(this).parents('.set-group').next();
          $('.set-group').hide("fast");
          next.stop().show("slide", { direction: "right" }, "normal");
        });
        $(".prev").click(function()
        {
          var prev = $(this).parents('.set-group').prev();
          $('.set-group').hide("fast");
          prev.stop().show("slide", { direction: "left" }, "normal");
        });

        
      });

    </script>

  </body>
</html>
