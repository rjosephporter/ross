<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_line_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function GetProductLines()
	{
		$this->db->where('status_id', 1);
		$query = $this->db->get('core_product_line');

		return $query->result_array();
	}

	function GetProductLineSegments($product_line_id = false)
	{
		$this->db->select('*');
		$this->db->from('core_product_line_segment');
		$this->db->where('status_id', 1);
		if($product_line_id != false) $this->db->where('product_line_id',$product_line_id);

		$query = $this->db->get();

		return $query->result_array();
	}

	function IsProductLineIdExist($product_line_id)
	{
		$this->db->from('core_product_line');
		$this->db->where('product_line_id', $product_line_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function IsProductLineSegmentIdExist($product_line_segment_id)
	{
		$this->db->from('core_product_line_segment');
		$this->db->where('product_line_segment_id', $product_line_segment_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}
}