<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Condition_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function IsConditionIdExist($condition_id)
	{
		$this->db->from('core_condition');
		$this->db->where('condition_id', $condition_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function GetConditions()
	{
		$this->db->where('status_id', 1);
		$query = $this->db->get('core_condition');

		return $query->result_array();
	}
}