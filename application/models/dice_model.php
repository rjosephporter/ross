<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dice_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function GetDiceProperties()
	{
		$query = $this->db->get('core_dice_property');

		return $query->result_array();
	}

	function GetDicePropertyOptions($dice_property_id = false)
	{
		$this->db->select('*');
		$this->db->from('core_dice_option');
		if($dice_property_id != false) $this->db->where('dice_property_id',$dice_property_id);

		$query = $this->db->get();

		return $query->result_array();
	}	

	function IsDicePropertyIdExist($dice_property_id)
	{
		$this->db->from('core_dice_property');
		$this->db->where('dice_property_id', $dice_property_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;

	}

	function IsDiceOptionIdExist($dice_option_id, $dice_property_id = false)
	{
		$this->db->from('core_dice_option');
		$this->db->where('dice_option_id', $dice_option_id);
		if($dice_property_id !== false) $this->db->where('dice_property_id', $dice_property_id);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function IsDicePropertyHasOption($dice_property_id)
	{
		$this->db->select('has_option');
		$this->db->from('core_dice_property');
		$this->db->where('dice_property_id', $dice_property_id);
		$this->db->limit('1');

		$query = $this->db->get();

		$has_option = $query->row();
		$has_option = $has_option->has_option;

		return ($has_option) ? true : false;

	}

	function GetDicePropertyCount()
	{
		$this->db->where('status_id', 1);

		return $this->db->count_all_results('core_dice_property');

	}
	
}