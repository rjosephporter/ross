<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function IsTestIdExist($test_id)
	{
		$this->db->from('core_test');
		$this->db->where('test_id', $test_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function GetTests()
	{
		$this->db->where('status_id', 1);
		$query = $this->db->get('core_test');

		return $query->result_array();
	}
}