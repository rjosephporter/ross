<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assembly_site_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function IsAssemblySiteIdExist($assembly_site_id)
	{
		$this->db->from('core_assembly_site');
		$this->db->where('assembly_site_id', $assembly_site_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function GetAssemblySites()
	{
		$this->db->where('status_id', 1);
		$query = $this->db->get('core_assembly_site');

		return $query->result_array();
	}
}