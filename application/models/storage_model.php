<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Storage_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function GetStorages($chamber_id = false) 
	{
		$this->db->from('core_storage');

		if($chamber_id !== false) 
			$this->db->where('chamber_id', $chamber_id);
		
		$this->db->where('status_id', 1);
		$this->db->order_by('storage_id');
		
		$query = $this->db->get();

		return $query->result_array();
	}

}