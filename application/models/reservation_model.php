<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservation_model extends CI_Model {

	/*
		TODO:
			- add filter for status_id
	*/

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function IsReservationIdExist($reservation_id)
	{
		$this->db->from('core_reservation');
		$this->db->where('reservation_id', $reservation_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function GetReservationDetails($reservation_id)
	{
		$this->db->select("
				r.*,
				concat(u.first_name,' ',u.last_name) as employee,
				p.name as package_name,
				ass.name as assembly_site_name,
				pl.name as product_line_name,
				pls.name as product_line_segment_name,
				cat.name as category_name,
				t.name as test_name,
				con.name as condition_name,
				ch.name as chamber_name
			",false);

		$this->db->from('core_reservation r');
		$this->db->join('core_user u','u.user_id = r.user_id','left');
		$this->db->join('core_package p','p.package_id = r.package_id','left');
		$this->db->join('core_assembly_site ass','ass.assembly_site_id = r.assembly_site_id','left');
		$this->db->join('core_product_line pl','pl.product_line_id = r.product_line_id','left');
		$this->db->join('core_product_line_segment pls','pls.product_line_segment_id = r.product_line_segment_id','left');
		$this->db->join('core_category cat','cat.category_id = r.category_id','left');
		$this->db->join('core_test t','t.test_id = r.test_id','left');
		$this->db->join('core_condition con','con.condition_id = r.condition_id','left');
		$this->db->join('core_chamber ch','ch.chamber_id = r.chamber_id','left');

		$this->db->where('r.reservation_id', $reservation_id);

		$this->db->limit(1);

		$query = $this->db->get();

		return $query->row_array();
	}

	function IsStorageAvailableByDate($storage_id_array, $start_datetime, $end_datetime)
	{

	}

	function GetStorageAvailable($test_id = false, $condition_id = false, $chamber_id = false, $start_datetime = '1000-01-01 00:00:00', $end_datetime = '9999-12-31 23:59:59')
	{

	}

	function GetStorageReservedByDate($start_datetime, $end_datetime, $chamber_id = false)
	{
		$this->db->select("
					s.storage_id,
					s.name,
					min(concat(r.start_date,' ',r.start_time)) as earliest_start_date,
					min(concat(r.end_date,' ',r.end_time)) as earliest_end_date
					",FALSE);
		$this->db->from("core_storage s");
		$this->db->join("core_reservation_storage rs","rs.storage_id = s.storage_id","left");
		$this->db->join("core_reservation r","r.reservation_id = rs.reservation_id","left");
		$this->db->where("s.status_id", 1);
		$this->db->where("s.chamber_id", $chamber_id);
		$this->db->where("concat(r.end_date,' ',r.end_time) >=", $start_datetime);
		$this->db->where("concat(r.start_date,' ',r.start_time) <=", $end_datetime);
		$this->db->group_by("s.storage_id");
		$this->db->order_by("s.storage_id");

		$query = $this->db->get();

		return $query->result_array();
	}

	/*
	*	Saves reservation details.
	*
	*	$data_array = array(
	*		'user_id' 					=> 123,
	*		'sequence_number' 			=> 123700,
	*		'qual_lot_number' 			=> 431213,
	*		'assembly_lot_number' 		=> 49854444,
	*		'rel_number' 				=> 745454222,
	*		'package_id' 				=> 6,
	*		'device' 					=> 'Samsung Galaxy S4',
	*		'datasheet_path' 			=> 'files/datasheet/file_001.doc',
	*		'qual_plan_path' 			=> 'files/qual_plan/file_002.pdf',
	*		'purpose' 					=> 'This is the purpose of the test.',
	*		'background' 				=> 'A detailed description of the test.',
	*		'assembly_site_id' 			=> 2,
	*		'product_line_id' 			=> 2,
	*		'product_line_segment_id' 	=> 5,
	*		'number_of_dice' 			=> 3,
	*		'category_id' 				=> 12,
	*		'test_id' 					=> 3,
	*		'condition_id' 				=> 7,
	*		'chamber_id' 				=> 16,
	*		'number_of_storage' 		=> 5,
	*		'start_date' 				=> '2013-11-29',
	*		'start_time' 				=> '17:00:00',
	*		'end_date' 					=> '2013-12-25',
	*		'end_time' 					=> '08:00:00'
	*	)
	*
	*	RETURNS $reservation_id if successful, otherwise FALSE
	*/
	function SaveReservation($data_array)	
	{
		$result = false;

		$this->db->trans_start();
		$this->db->insert('core_reservation', $data_array);
		$this->db->trans_complete();

		if($this->db->affected_rows() === 1) {
			$result = $this->db->insert_id();
		}

		return $result;
	}

	/*
	*	Saves reservation's dice properties and options.
	*
	*	$dice_array = array(
	*					array(
	*						'dice_property_id'	=> 1,
	*						'dice_number'		=> 1,
	*						'dice_option_id'	=> 7,
	*					),
	*					array(
	*						'dice_property_id'	=> 1,
	*						'dice_number'		=> 2,
	*						'dice_option_id'	=> 4,
	*					),
	*					array(
	*						'dice_property_id'	=> 1,
	*						'dice_number'		=> 3,
	*						'dice_option_id'	=> 6,
	*					),
	*
	*					array(
	*						'dice_property_id'	=> 2,
	*						'dice_number'		=> 1,
	*						'dice_option_id'	=> 9,
	*					),
	*					array(
	*						'dice_property_id'	=> 2,
	*						'dice_number'		=> 2,
	*						'dice_option_id'	=> 10,
	*					),
	*					array(
	*						'dice_property_id'	=> 2,
	*						'dice_number'		=> 3,
	*						'dice_option_id'	=> 8,
	*					),
	*
	*					array(
	*						'dice_property_id'	=> 3,
	*						'dice_number'		=> 1,
	*						'text'				=> 'die run test 4564654',
	*					),
	*					array(
	*						'dice_property_id'	=> 3,
	*						'dice_number'		=> 2,
	*						'text'				=> 'die run test 121516',
	*					),
	*					array(
	*						'dice_property_id'	=> 3,
	*						'dice_number'		=> 3,
	*						'text'				=> 'die run test 74654652',
	*					),
	*
	*					array(
	*						'dice_property_id'	=> 4,
	*						'dice_number'		=> 1,
	*						'text'				=> 'wafer no. 4564654',
	*					),
	*					array(
	*						'dice_property_id'	=> 4,
	*						'dice_number'		=> 2,
	*						'text'				=> 'wafer no. 121516',
	*					),
	*					array(
	*						'dice_property_id'	=> 4,
	*						'dice_number'		=> 3,
	*						'text'				=> 'wafer no. 74654652',
	*					)
	*	)
	*
	*	
	*
	*/
	function SaveReservationDice($reservation_id, $dice_array)
	{
		$result = false;
		$insert_count = 0;

		$this->db->trans_start();

		foreach($dice_array as $option) {
			$option['reservation_id'] = $reservation_id;
			foreach($option as $key => $value){
				$this->db->set($key,$value);
			}
			$this->db->insert('core_reservation_dice');
			if($this->db->affected_rows() === count($option)) $insert_count = $insert_count + count($option);
		}		
				
		$this->db->trans_complete();

		if($insert_count === count($dice_array)) {
			$result = true;
		}

		return $result;
		
	}

	function SaveReservationStorage($reservation_id, $storage_id_array)
	{
		$result = false;
		$data[] = array();
		$ctr = 0;

		foreach($storage_id_array as $storage_id) {
			$data[$ctr]['reservation_id'] = $reservation_id;
			$data[$ctr]['storage_id'] = $storage_id;
			$ctr++;
		}

		$this->db->trans_start();
		$this->db->insert_batch('core_reservation_storage', $data);
		$this->db->trans_complete();

		if($this->db->affected_rows() === count($storage_id_array)) {
			$result = true;
		}

		return $result;

	}

	function GetReservationCalendar($start_datetime, $end_datetime, $filter_array = false)
	{
		$this->db->select("
				r.*,
				concat(u.first_name,' ',u.last_name) as employee,
				p.name as package_name,
				ass.name as assembly_site_name,
				pl.name as product_line_name,
				pls.name as product_line_segment_name,
				cat.name as category_name,
				t.name as test_name,
				con.name as condition_name,
				ch.name as chamber_name
			",false);

		$this->db->from('core_reservation r');
		$this->db->join('core_user u','u.user_id = r.user_id','left');
		$this->db->join('core_package p','p.package_id = r.package_id','left');
		$this->db->join('core_assembly_site ass','ass.assembly_site_id = r.assembly_site_id','left');
		$this->db->join('core_product_line pl','pl.product_line_id = r.product_line_id','left');
		$this->db->join('core_product_line_segment pls','pls.product_line_segment_id = r.product_line_segment_id','left');
		$this->db->join('core_category cat','cat.category_id = r.category_id','left');
		$this->db->join('core_test t','t.test_id = r.test_id','left');
		$this->db->join('core_condition con','con.condition_id = r.condition_id','left');
		$this->db->join('core_chamber ch','ch.chamber_id = r.chamber_id','left');

		$this->db->where('r.status_id', 1);
		$this->db->where("concat(r.start_date,' ',r.start_time) >=", $start_datetime);
		$this->db->where("concat(r.start_date,' ',r.start_time) <=", $end_datetime);
		if($filter_array !== false) {
			foreach($filter_array as $key => $value)
				$this->db->where('r.'.$key.' in ', '('.$value.')', false);
		}

		$query = $this->db->get();

		return $query->result_array();
	}

}