<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Package_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function IsPackageIdExist($package_id)
	{
		$this->db->from('core_package');
		$this->db->where('package_id', $package_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function GetPackages()
	{
		$this->db->where('status_id', 1);
		$query = $this->db->get('core_package');

		return $query->result_array();
	}
}