<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function IsCategoryIdExist($category_id)
	{
		$this->db->from('core_category');
		$this->db->where('category_id', $category_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function GetCategories()
	{
		$this->db->where('status_id', 1);
		$query = $this->db->get('core_category');

		return $query->result_array();
	}
}