<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chamber_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();		
	}

	function IsChamberIdExist($chamber_id)
	{
		$this->db->from('core_chamber');
		$this->db->where('chamber_id', $chamber_id);
		$this->db->where('status_id', 1);

		$count = $this->db->count_all_results();
		return ($count > 0) ? true : false;
	}

	function GetChambers()
	{
		$this->db->where('status_id', 1);
		$query = $this->db->get('core_chamber');

		return $query->result_array();
	}
}