/*
Navicat MySQL Data Transfer

Source Server         : LOCAL
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : ross

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2013-12-01 23:20:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for core_assembly_site
-- ----------------------------
DROP TABLE IF EXISTS `core_assembly_site`;
CREATE TABLE `core_assembly_site` (
  `assembly_site_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`assembly_site_id`),
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_assembly_site
-- ----------------------------
INSERT INTO `core_assembly_site` VALUES ('1', 'FSCP', null, '2013-11-28 18:01:03', '1');
INSERT INTO `core_assembly_site` VALUES ('2', 'FSSZ', null, '2013-11-28 18:01:12', '1');
INSERT INTO `core_assembly_site` VALUES ('3', 'FSPM', null, '2013-11-28 18:01:22', '1');

-- ----------------------------
-- Table structure for core_category
-- ----------------------------
DROP TABLE IF EXISTS `core_category`;
CREATE TABLE `core_category` (
  `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`category_id`),
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_category
-- ----------------------------
INSERT INTO `core_category` VALUES ('1', 'Assembly/Subcon', null, null, '2013-11-28 14:14:18', '1');
INSERT INTO `core_category` VALUES ('2', 'RMP', null, null, '2013-11-28 14:14:38', '1');
INSERT INTO `core_category` VALUES ('3', 'Competitor Evaluations', null, null, '2013-11-28 14:19:28', '1');
INSERT INTO `core_category` VALUES ('4', 'Customer Evaluations', null, null, '2013-11-28 14:19:45', '1');
INSERT INTO `core_category` VALUES ('5', 'Buy-off (Process evaluations)', null, null, '2013-11-28 14:21:38', '1');
INSERT INTO `core_category` VALUES ('6', 'Buy-off (Risk assessments)', null, null, '2013-11-28 14:21:47', '1');
INSERT INTO `core_category` VALUES ('7', 'Buy-off (DOE)', null, null, '2013-11-28 14:21:55', '1');
INSERT INTO `core_category` VALUES ('8', 'Process Improvements (Qualifications with EQPP)', null, null, '2013-11-28 14:22:02', '1');
INSERT INTO `core_category` VALUES ('9', 'New Product (PQFN and Clips BU)', null, null, '2013-11-28 14:22:09', '1');
INSERT INTO `core_category` VALUES ('10', 'New Product (Automotive & PPG BU)', null, null, '2013-11-28 14:22:20', '1');
INSERT INTO `core_category` VALUES ('11', 'New Product (Matured packages BU)', null, null, '2013-11-28 14:22:28', '1');
INSERT INTO `core_category` VALUES ('12', 'New Package', null, null, '2013-11-28 14:22:35', '1');
INSERT INTO `core_category` VALUES ('13', 'Test-to-Fail', null, null, '2013-11-28 14:22:44', '1');

-- ----------------------------
-- Table structure for core_chamber
-- ----------------------------
DROP TABLE IF EXISTS `core_chamber`;
CREATE TABLE `core_chamber` (
  `chamber_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`chamber_id`),
  UNIQUE KEY `ndx_name` (`name`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_chamber
-- ----------------------------
INSERT INTO `core_chamber` VALUES ('1', '#16', null, null, '2013-11-28 14:41:48', '1');
INSERT INTO `core_chamber` VALUES ('2', '#46', null, null, '2013-11-28 14:42:10', '1');
INSERT INTO `core_chamber` VALUES ('4', '#20', null, null, '2013-11-28 14:42:34', '1');
INSERT INTO `core_chamber` VALUES ('5', '#29', null, null, '2013-11-28 14:42:51', '1');
INSERT INTO `core_chamber` VALUES ('6', '#35', null, null, '2013-11-28 14:43:01', '1');
INSERT INTO `core_chamber` VALUES ('7', '#47', null, null, '2013-11-28 14:43:21', '1');
INSERT INTO `core_chamber` VALUES ('8', '#36', null, null, '2013-11-28 14:43:56', '1');
INSERT INTO `core_chamber` VALUES ('9', '#14', null, null, '2013-11-28 14:44:04', '1');
INSERT INTO `core_chamber` VALUES ('10', '#64', null, null, '2013-11-28 14:44:10', '1');
INSERT INTO `core_chamber` VALUES ('11', '#65', null, null, '2013-11-28 14:44:24', '1');
INSERT INTO `core_chamber` VALUES ('12', '#48', null, null, '2013-11-28 14:44:30', '1');
INSERT INTO `core_chamber` VALUES ('13', '#49', null, null, '2013-11-28 14:44:37', '1');
INSERT INTO `core_chamber` VALUES ('16', '#98', null, null, '2013-11-28 15:27:50', '1');
INSERT INTO `core_chamber` VALUES ('17', '#26', null, null, '2013-11-28 15:28:14', '1');
INSERT INTO `core_chamber` VALUES ('18', '#31', null, null, '2013-11-28 15:28:19', '1');
INSERT INTO `core_chamber` VALUES ('19', '#37', null, null, '2013-11-28 15:28:25', '1');
INSERT INTO `core_chamber` VALUES ('20', '#4', null, null, '2013-11-28 15:28:29', '1');
INSERT INTO `core_chamber` VALUES ('21', '#69', null, null, '2013-11-28 15:28:32', '1');
INSERT INTO `core_chamber` VALUES ('22', '#70', null, null, '2013-11-28 15:28:36', '1');
INSERT INTO `core_chamber` VALUES ('23', '#57', null, null, '2013-11-28 15:28:41', '1');
INSERT INTO `core_chamber` VALUES ('25', '#45', null, null, '2013-11-28 15:28:58', '1');
INSERT INTO `core_chamber` VALUES ('27', '#30', null, null, '2013-11-28 15:29:56', '1');
INSERT INTO `core_chamber` VALUES ('28', '#9', null, null, '2013-11-28 15:29:59', '1');
INSERT INTO `core_chamber` VALUES ('29', '#18', null, null, '2013-11-28 15:30:04', '1');
INSERT INTO `core_chamber` VALUES ('30', '#23', null, null, '2013-11-28 15:30:08', '1');
INSERT INTO `core_chamber` VALUES ('31', '#24', null, null, '2013-11-28 15:30:12', '1');
INSERT INTO `core_chamber` VALUES ('32', '#32', null, null, '2013-11-28 15:30:17', '1');
INSERT INTO `core_chamber` VALUES ('33', '#33', null, null, '2013-11-28 15:30:19', '1');
INSERT INTO `core_chamber` VALUES ('34', '#34', null, null, '2013-11-28 15:30:24', '1');
INSERT INTO `core_chamber` VALUES ('35', '#38', null, null, '2013-11-28 15:30:26', '1');
INSERT INTO `core_chamber` VALUES ('36', '#39', null, null, '2013-11-28 15:30:31', '1');
INSERT INTO `core_chamber` VALUES ('37', '#40', null, null, '2013-11-28 15:30:35', '1');
INSERT INTO `core_chamber` VALUES ('38', '#99', null, null, '2013-11-28 15:30:39', '1');
INSERT INTO `core_chamber` VALUES ('39', '#51', null, null, '2013-11-28 15:30:41', '1');
INSERT INTO `core_chamber` VALUES ('40', '#52', null, null, '2013-11-28 15:30:46', '1');
INSERT INTO `core_chamber` VALUES ('41', '#53', null, null, '2013-11-28 15:30:49', '1');
INSERT INTO `core_chamber` VALUES ('42', '#54', null, null, '2013-11-28 15:30:52', '1');
INSERT INTO `core_chamber` VALUES ('43', '#55', null, null, '2013-11-28 15:30:54', '1');
INSERT INTO `core_chamber` VALUES ('44', '#56', null, null, '2013-11-28 15:31:00', '1');
INSERT INTO `core_chamber` VALUES ('45', '#62', null, null, '2013-11-28 15:31:26', '1');
INSERT INTO `core_chamber` VALUES ('46', '#11', null, null, '2013-11-28 15:31:38', '1');
INSERT INTO `core_chamber` VALUES ('47', '#28', null, null, '2013-11-28 15:31:43', '1');
INSERT INTO `core_chamber` VALUES ('48', '#15', null, null, '2013-11-28 15:31:47', '1');
INSERT INTO `core_chamber` VALUES ('49', '#50', null, null, '2013-11-28 15:31:51', '1');
INSERT INTO `core_chamber` VALUES ('50', '#21', null, null, '2013-11-28 15:31:56', '1');

-- ----------------------------
-- Table structure for core_condition
-- ----------------------------
DROP TABLE IF EXISTS `core_condition`;
CREATE TABLE `core_condition` (
  `condition_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`condition_id`),
  UNIQUE KEY `ndx_name` (`name`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_condition
-- ----------------------------
INSERT INTO `core_condition` VALUES ('1', '125°C / VCC=?', null, null, '2013-11-28 14:30:26', '1');
INSERT INTO `core_condition` VALUES ('3', '150°C / VCC=?', null, null, '2013-11-28 14:30:58', '1');
INSERT INTO `core_condition` VALUES ('4', '175°C / VCC=?', null, null, '2013-11-28 14:31:09', '1');
INSERT INTO `core_condition` VALUES ('7', ' 110°C/85%RH / VCC=?', null, null, '2013-11-28 14:31:48', '1');
INSERT INTO `core_condition` VALUES ('8', ' 130°C/85%RH / VCC=?', null, null, '2013-11-28 14:32:10', '1');
INSERT INTO `core_condition` VALUES ('9', '-65°C/+150°C', null, null, '2013-11-28 14:32:35', '1');
INSERT INTO `core_condition` VALUES ('10', '-55°C/150°C', null, null, '2013-11-28 14:32:44', '1');
INSERT INTO `core_condition` VALUES ('11', 'MSL1', null, null, '2013-11-28 14:33:23', '1');
INSERT INTO `core_condition` VALUES ('12', 'MSL3', null, null, '2013-11-28 14:33:30', '1');
INSERT INTO `core_condition` VALUES ('13', '-40°C/125°C', null, null, '2013-11-28 14:33:43', '1');
INSERT INTO `core_condition` VALUES ('14', '-55°C/+125°C or -55°c /150°c', null, null, '2013-11-28 14:34:21', '1');
INSERT INTO `core_condition` VALUES ('15', '2mins on/off V1 =?  / V2 = ?', null, null, '2013-11-28 14:35:45', '1');
INSERT INTO `core_condition` VALUES ('16', '2mins on / off VDD=? / Vref =? / I=?, 3mins on/off VDD=? / Vref=? / I=? &3.5mins on / off VDD=? / Vref=? / I=?', null, null, '2013-11-28 14:36:40', '1');
INSERT INTO `core_condition` VALUES ('17', '110°c V1 = ? / V2 = ? & 125°C V1=? / V2=?', null, null, '2013-11-28 14:36:55', '1');
INSERT INTO `core_condition` VALUES ('18', '121°C/ 15PSI / 100%RH', null, null, '2013-11-28 14:37:05', '1');
INSERT INTO `core_condition` VALUES ('19', '85°C/85%RH / VCC=?', null, null, '2013-11-28 14:37:16', '1');
INSERT INTO `core_condition` VALUES ('20', '125°C/ V = ? / I=?', null, null, '2013-11-28 14:37:26', '1');

-- ----------------------------
-- Table structure for core_condition_chamber
-- ----------------------------
DROP TABLE IF EXISTS `core_condition_chamber`;
CREATE TABLE `core_condition_chamber` (
  `condition_chamber_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `condition_id` bigint(20) unsigned NOT NULL,
  `chamber_id` bigint(20) unsigned NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`condition_chamber_id`),
  UNIQUE KEY `ndx_condition_chamber_id` (`condition_id`,`chamber_id`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE,
  KEY `ndx_chamber_id` (`chamber_id`) USING BTREE,
  CONSTRAINT `core_condition_chamber_ibfk_1` FOREIGN KEY (`chamber_id`) REFERENCES `core_chamber` (`chamber_id`),
  CONSTRAINT `core_condition_chamber_ibfk_2` FOREIGN KEY (`condition_id`) REFERENCES `core_condition` (`condition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_condition_chamber
-- ----------------------------
INSERT INTO `core_condition_chamber` VALUES ('1', '1', '1', '2013-11-28 16:00:46', '1');
INSERT INTO `core_condition_chamber` VALUES ('2', '1', '2', '2013-11-28 16:00:48', '1');
INSERT INTO `core_condition_chamber` VALUES ('3', '3', '4', '2013-11-28 16:01:22', '1');
INSERT INTO `core_condition_chamber` VALUES ('4', '3', '5', '2013-11-28 16:01:27', '1');
INSERT INTO `core_condition_chamber` VALUES ('5', '3', '6', '2013-11-28 16:01:32', '1');
INSERT INTO `core_condition_chamber` VALUES ('6', '3', '7', '2013-11-28 16:01:37', '1');
INSERT INTO `core_condition_chamber` VALUES ('7', '4', '8', '2013-11-28 16:18:07', '1');
INSERT INTO `core_condition_chamber` VALUES ('8', '4', '9', '2013-11-28 16:18:13', '1');
INSERT INTO `core_condition_chamber` VALUES ('9', '4', '10', '2013-11-28 16:18:16', '1');
INSERT INTO `core_condition_chamber` VALUES ('10', '4', '11', '2013-11-28 16:18:22', '1');
INSERT INTO `core_condition_chamber` VALUES ('11', '4', '12', '2013-11-28 16:18:27', '1');
INSERT INTO `core_condition_chamber` VALUES ('12', '4', '13', '2013-11-28 16:18:33', '1');
INSERT INTO `core_condition_chamber` VALUES ('13', '7', '16', '2013-11-28 16:31:39', '1');
INSERT INTO `core_condition_chamber` VALUES ('15', '8', '17', '2013-11-28 16:33:41', '1');
INSERT INTO `core_condition_chamber` VALUES ('16', '8', '18', '2013-11-28 16:34:26', '1');
INSERT INTO `core_condition_chamber` VALUES ('17', '9', '19', '2013-11-28 16:35:05', '1');
INSERT INTO `core_condition_chamber` VALUES ('18', '10', '20', '2013-11-28 16:35:42', '1');
INSERT INTO `core_condition_chamber` VALUES ('19', '10', '21', '2013-11-28 16:36:06', '1');
INSERT INTO `core_condition_chamber` VALUES ('20', '10', '22', '2013-11-28 16:36:18', '1');
INSERT INTO `core_condition_chamber` VALUES ('21', '11', '23', '2013-11-28 16:37:44', '1');
INSERT INTO `core_condition_chamber` VALUES ('22', '11', '1', '2013-11-28 16:38:10', '1');
INSERT INTO `core_condition_chamber` VALUES ('23', '11', '25', '2013-11-28 16:38:41', '1');
INSERT INTO `core_condition_chamber` VALUES ('24', '12', '23', '2013-11-28 16:38:56', '1');
INSERT INTO `core_condition_chamber` VALUES ('25', '12', '1', '2013-11-28 16:38:58', '1');
INSERT INTO `core_condition_chamber` VALUES ('26', '12', '25', '2013-11-28 16:39:01', '1');
INSERT INTO `core_condition_chamber` VALUES ('27', '13', '23', '2013-11-28 16:41:40', '1');
INSERT INTO `core_condition_chamber` VALUES ('28', '14', '27', '2013-11-28 16:43:16', '1');
INSERT INTO `core_condition_chamber` VALUES ('29', '15', '28', '2013-11-28 16:43:39', '1');
INSERT INTO `core_condition_chamber` VALUES ('30', '16', '29', '2013-11-28 16:44:33', '1');
INSERT INTO `core_condition_chamber` VALUES ('31', '16', '30', '2013-11-28 16:44:45', '1');
INSERT INTO `core_condition_chamber` VALUES ('32', '16', '31', '2013-11-28 16:44:59', '1');
INSERT INTO `core_condition_chamber` VALUES ('33', '16', '32', '2013-11-28 16:45:38', '1');
INSERT INTO `core_condition_chamber` VALUES ('34', '16', '33', '2013-11-28 16:46:02', '1');
INSERT INTO `core_condition_chamber` VALUES ('35', '16', '34', '2013-11-28 16:46:13', '1');
INSERT INTO `core_condition_chamber` VALUES ('36', '16', '35', '2013-11-28 16:46:23', '1');
INSERT INTO `core_condition_chamber` VALUES ('37', '16', '36', '2013-11-28 16:47:28', '1');
INSERT INTO `core_condition_chamber` VALUES ('38', '16', '37', '2013-11-28 16:47:39', '1');
INSERT INTO `core_condition_chamber` VALUES ('39', '16', '38', '2013-11-28 16:47:48', '1');
INSERT INTO `core_condition_chamber` VALUES ('40', '16', '39', '2013-11-28 16:47:58', '1');
INSERT INTO `core_condition_chamber` VALUES ('41', '16', '40', '2013-11-28 16:48:16', '1');
INSERT INTO `core_condition_chamber` VALUES ('42', '16', '41', '2013-11-28 16:48:28', '1');
INSERT INTO `core_condition_chamber` VALUES ('43', '16', '42', '2013-11-28 16:48:40', '1');
INSERT INTO `core_condition_chamber` VALUES ('44', '16', '43', '2013-11-28 16:48:58', '1');
INSERT INTO `core_condition_chamber` VALUES ('45', '16', '44', '2013-11-28 16:49:22', '1');
INSERT INTO `core_condition_chamber` VALUES ('46', '17', '45', '2013-11-28 16:49:44', '1');
INSERT INTO `core_condition_chamber` VALUES ('47', '18', '46', '2013-11-28 16:50:06', '1');
INSERT INTO `core_condition_chamber` VALUES ('48', '18', '47', '2013-11-28 16:50:16', '1');
INSERT INTO `core_condition_chamber` VALUES ('49', '19', '48', '2013-11-28 16:50:39', '1');
INSERT INTO `core_condition_chamber` VALUES ('50', '19', '49', '2013-11-28 16:50:51', '1');
INSERT INTO `core_condition_chamber` VALUES ('51', '20', '50', '2013-11-28 16:51:03', '1');

-- ----------------------------
-- Table structure for core_department
-- ----------------------------
DROP TABLE IF EXISTS `core_department`;
CREATE TABLE `core_department` (
  `department_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`department_id`),
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_department
-- ----------------------------

-- ----------------------------
-- Table structure for core_dice_option
-- ----------------------------
DROP TABLE IF EXISTS `core_dice_option`;
CREATE TABLE `core_dice_option` (
  `dice_option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dice_property_id` bigint(20) unsigned NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`dice_option_id`),
  KEY `ndx_dice_property_id` (`dice_property_id`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE,
  CONSTRAINT `core_dice_option_ibfk_1` FOREIGN KEY (`dice_property_id`) REFERENCES `core_dice_property` (`dice_property_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_dice_option
-- ----------------------------
INSERT INTO `core_dice_option` VALUES ('1', '1', 'FSSL', null, null, '2013-11-29 10:24:26', '1');
INSERT INTO `core_dice_option` VALUES ('2', '1', 'FSME', null, null, '2013-11-29 10:24:32', '1');
INSERT INTO `core_dice_option` VALUES ('3', '1', 'TSMC', null, null, '2013-11-29 10:24:36', '1');
INSERT INTO `core_dice_option` VALUES ('4', '1', 'Vanguard', null, null, '2013-11-29 10:24:43', '1');
INSERT INTO `core_dice_option` VALUES ('5', '1', 'MTOP', null, null, '2013-11-29 10:24:49', '1');
INSERT INTO `core_dice_option` VALUES ('6', '1', 'FSBK', null, null, '2013-11-29 10:25:04', '1');
INSERT INTO `core_dice_option` VALUES ('7', '1', 'Jazz', null, null, '2013-11-29 10:25:09', '1');
INSERT INTO `core_dice_option` VALUES ('8', '2', 'Option 1', null, null, '2013-11-29 10:25:17', '1');
INSERT INTO `core_dice_option` VALUES ('9', '2', 'Option 2', null, null, '2013-11-29 10:25:22', '1');
INSERT INTO `core_dice_option` VALUES ('10', '2', 'Option 3', null, null, '2013-11-29 10:25:27', '1');

-- ----------------------------
-- Table structure for core_dice_property
-- ----------------------------
DROP TABLE IF EXISTS `core_dice_property`;
CREATE TABLE `core_dice_property` (
  `dice_property_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `has_option` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = no, 1 = yes',
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`dice_property_id`),
  UNIQUE KEY `ndx_name` (`name`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_dice_property
-- ----------------------------
INSERT INTO `core_dice_property` VALUES ('1', 'Fab Site', null, null, '2013-11-29 10:23:08', '1', '1');
INSERT INTO `core_dice_property` VALUES ('2', 'Die Technology', null, null, '2013-11-29 10:23:23', '1', '1');
INSERT INTO `core_dice_property` VALUES ('3', 'Die Run', null, null, '2013-11-29 10:23:31', '0', '1');
INSERT INTO `core_dice_property` VALUES ('4', 'Wafer No.', null, null, '2013-11-29 10:23:38', '0', '1');

-- ----------------------------
-- Table structure for core_package
-- ----------------------------
DROP TABLE IF EXISTS `core_package`;
CREATE TABLE `core_package` (
  `package_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`package_id`),
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_package
-- ----------------------------
INSERT INTO `core_package` VALUES ('1', 'SOT23', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('2', 'SOT23-5', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('3', 'SSOT-3', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('4', 'SSOT-6', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('5', 'TO-263', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('6', 'TO-252', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('7', 'TO-220', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('8', 'TO-220F', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('9', 'WLCSP', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('10', 'PQFN 5x6 Standard', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('11', 'PQFN 3x3 Standard', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('12', 'PQFN 6x6 DrMOS', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('13', 'PQFN 5x5 Tinybuck3', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('14', 'SO-5 Opto', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('15', 'SO-16 Opto', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('16', 'PQFN 3x3 Clipbond', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('17', 'PQFN 3x3 Dual Cool', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('18', 'PQFN 5x6 Clipbond', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('19', 'PQFN 3X5', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('20', 'PQFN 8x8 ', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('21', 'PQFN 5x6 Dual Cool', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('22', 'PQFN 3x3 Dual Common Drain', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('23', 'PowerClip 3X5', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('24', 'PowerClip 5x6', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('25', 'PowerClip 3x3 Singles', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('26', 'PowerClip 3x3 Duals', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('27', 'PQFN 5x6 Dual Asymmetric', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('28', 'PQFN 5x6 Automotive', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('29', 'Smart Power Stage 5x5', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('30', 'PowerClip 5x6 Multiphase', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('31', 'SC70-5', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('32', 'SC70-6', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('33', 'Die Sales', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('34', 'Wafer Sales', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('35', 'KGD', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('36', 'SPM7', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('37', 'LTS', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('38', 'TOLL', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('39', 'SPS56 DC', null, '2013-11-28 17:49:24', '1');
INSERT INTO `core_package` VALUES ('40', 'DrMOS', null, '2013-11-28 17:49:24', '1');

-- ----------------------------
-- Table structure for core_position
-- ----------------------------
DROP TABLE IF EXISTS `core_position`;
CREATE TABLE `core_position` (
  `position_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`position_id`),
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_position
-- ----------------------------

-- ----------------------------
-- Table structure for core_product_line
-- ----------------------------
DROP TABLE IF EXISTS `core_product_line`;
CREATE TABLE `core_product_line` (
  `product_line_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `other_info` varchar(255) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`product_line_id`),
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_product_line
-- ----------------------------
INSERT INTO `core_product_line` VALUES ('1', 'MCCC', '', '2013-11-28 18:03:22', '1');
INSERT INTO `core_product_line` VALUES ('2', 'PCIA', '', '2013-11-28 18:03:30', '1');

-- ----------------------------
-- Table structure for core_product_line_segment
-- ----------------------------
DROP TABLE IF EXISTS `core_product_line_segment`;
CREATE TABLE `core_product_line_segment` (
  `product_line_segment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_line_id` bigint(20) unsigned NOT NULL,
  `name` varchar(150) NOT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`product_line_segment_id`),
  KEY `ndx_status_id` (`status_id`) USING BTREE,
  KEY `ndx_product_line_id` (`product_line_id`) USING BTREE,
  CONSTRAINT `fk_cpls_product_line_id` FOREIGN KEY (`product_line_id`) REFERENCES `core_product_line` (`product_line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_product_line_segment
-- ----------------------------
INSERT INTO `core_product_line_segment` VALUES ('1', '1', 'LV', null, '2013-11-28 18:05:28', '1');
INSERT INTO `core_product_line_segment` VALUES ('2', '1', 'Mobile', null, '2013-11-28 18:05:37', '1');
INSERT INTO `core_product_line_segment` VALUES ('3', '1', 'Mid Power Analog', null, '2013-11-28 18:05:59', '1');
INSERT INTO `core_product_line_segment` VALUES ('4', '2', 'Automotive', null, '2013-11-28 18:06:07', '1');
INSERT INTO `core_product_line_segment` VALUES ('5', '2', 'Power conversion', null, '2013-11-28 18:06:15', '1');
INSERT INTO `core_product_line_segment` VALUES ('6', '2', 'Opto', null, '2013-11-28 18:06:20', '1');
INSERT INTO `core_product_line_segment` VALUES ('7', '2', 'HV', null, '2013-11-28 18:06:40', '1');

-- ----------------------------
-- Table structure for core_reservation
-- ----------------------------
DROP TABLE IF EXISTS `core_reservation`;
CREATE TABLE `core_reservation` (
  `reservation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `sequence_number` bigint(20) NOT NULL,
  `qual_lot_number` bigint(20) NOT NULL,
  `assembly_lot_number` bigint(20) NOT NULL,
  `rel_number` bigint(20) DEFAULT NULL COMMENT 'inputted by admin',
  `package_id` bigint(20) unsigned NOT NULL,
  `device` varchar(150) DEFAULT NULL,
  `datasheet_path` varchar(255) DEFAULT NULL,
  `qual_plan_path` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `background` text,
  `assembly_site_id` bigint(20) unsigned NOT NULL,
  `product_line_id` bigint(20) unsigned NOT NULL,
  `product_line_segment_id` bigint(20) unsigned NOT NULL,
  `number_of_dice` int(11) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `test_id` bigint(20) unsigned NOT NULL,
  `condition_id` bigint(20) unsigned NOT NULL,
  `chamber_id` bigint(20) unsigned NOT NULL,
  `number_of_storage` int(11) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` datetime DEFAULT NULL,
  `approved_by_id` bigint(20) unsigned DEFAULT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-2 = revoked, -1 = cancelled, 0 = pending, 1 = active, 2 = completed',
  PRIMARY KEY (`reservation_id`),
  KEY `ndx_user_id` (`user_id`) USING BTREE,
  KEY `ndx_sequence_number` (`sequence_number`) USING BTREE,
  KEY `ndx_package_id` (`package_id`) USING BTREE,
  KEY `ndx_assembly_site_id` (`assembly_site_id`) USING BTREE,
  KEY `ndx_product_line_id` (`product_line_id`) USING BTREE,
  KEY `ndx_product_line_segment_id` (`product_line_segment_id`) USING BTREE,
  KEY `ndx_category_id` (`category_id`) USING BTREE,
  KEY `ndx_test_id` (`test_id`) USING BTREE,
  KEY `ndx_condition_id` (`condition_id`) USING BTREE,
  KEY `ndx_chamber_id` (`chamber_id`) USING BTREE,
  KEY `ndx_approved_by_id` (`approved_by_id`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_reservation
-- ----------------------------
INSERT INTO `core_reservation` VALUES ('1', '1', '1001', '12352', '21215', '54621', '1', 'dsfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', '1', '1', '1', '3', '1', '1', '1', '1', '4', '2013-12-01', '14:00:00', '2013-12-06', '13:00:00', '2013-12-01 13:48:12', null, null, '1');
INSERT INTO `core_reservation` VALUES ('2', '123', '123700', '431213', '49854444', '745454222', '6', 'Samsung Galaxy S4', 'files/datasheet/file_001.doc', 'files/qual_plan/file_002.pdf', 'This is the purpose of the test.', 'A detailed description of the test.', '2', '2', '5', '3', '12', '3', '7', '16', '5', '2013-11-29', '17:00:00', '2013-11-30', '08:00:00', null, null, null, '1');
INSERT INTO `core_reservation` VALUES ('3', '123', '123700', '54654', '654654', '65465', '1', 'galaxy tab', 'path1', 'path2', 'purposing', 'backgrounding', '2', '2', '5', '3', '1', '1', '1', '1', '5', '2013-12-09', '18:00:00', '2013-12-12', '07:00:00', '2013-12-01 18:21:49', null, null, '1');

-- ----------------------------
-- Table structure for core_reservation_dice
-- ----------------------------
DROP TABLE IF EXISTS `core_reservation_dice`;
CREATE TABLE `core_reservation_dice` (
  `reservation_dice_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` bigint(20) unsigned NOT NULL,
  `dice_property_id` bigint(20) unsigned NOT NULL,
  `dice_number` int(11) unsigned NOT NULL,
  `dice_option_id` bigint(20) unsigned DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`reservation_dice_id`),
  KEY `ndx_reservation_id` (`reservation_id`) USING BTREE,
  KEY `ndx_dice_property_id` (`dice_property_id`) USING BTREE,
  KEY `ndx_dice_option_id` (`dice_option_id`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE,
  CONSTRAINT `core_reservation_dice_ibfk_1` FOREIGN KEY (`dice_option_id`) REFERENCES `core_dice_option` (`dice_option_id`),
  CONSTRAINT `core_reservation_dice_ibfk_2` FOREIGN KEY (`dice_property_id`) REFERENCES `core_dice_property` (`dice_property_id`),
  CONSTRAINT `core_reservation_dice_ibfk_3` FOREIGN KEY (`reservation_id`) REFERENCES `core_reservation` (`reservation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_reservation_dice
-- ----------------------------
INSERT INTO `core_reservation_dice` VALUES ('28', '1', '1', '1', '7', null, '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('29', '1', '1', '2', '4', null, '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('30', '1', '1', '3', '6', null, '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('31', '1', '2', '1', '9', null, '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('32', '1', '2', '2', '10', null, '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('33', '1', '2', '3', '8', null, '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('34', '1', '3', '1', null, 'die run test 4564654', '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('35', '1', '3', '2', null, 'die run test 121516', '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('36', '1', '3', '3', null, 'die run test 74654652', '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('37', '1', '4', '1', null, 'wafer no. 4564654', '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('38', '1', '4', '2', null, 'wafer no. 121516', '2013-12-01 14:16:18', '1');
INSERT INTO `core_reservation_dice` VALUES ('39', '1', '4', '3', null, 'wafer no. 74654652', '2013-12-01 14:16:18', '1');

-- ----------------------------
-- Table structure for core_reservation_storage
-- ----------------------------
DROP TABLE IF EXISTS `core_reservation_storage`;
CREATE TABLE `core_reservation_storage` (
  `reservation_storage_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` bigint(20) unsigned NOT NULL,
  `storage_id` bigint(20) unsigned NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` datetime DEFAULT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-2 = revoked, -1 = cancelled, 0 = pending, 1 = active, 2 = completed',
  PRIMARY KEY (`reservation_storage_id`),
  UNIQUE KEY `ndx_reservation_storage_id` (`reservation_id`,`storage_id`) USING BTREE,
  KEY `ndx_reservation_id` (`reservation_id`) USING BTREE,
  KEY `ndx_storage_id` (`storage_id`) USING BTREE,
  CONSTRAINT `fk_crs_reservation_id` FOREIGN KEY (`reservation_id`) REFERENCES `core_reservation` (`reservation_id`),
  CONSTRAINT `fk_crs_storage_id` FOREIGN KEY (`storage_id`) REFERENCES `core_storage` (`storage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_reservation_storage
-- ----------------------------
INSERT INTO `core_reservation_storage` VALUES ('5', '1', '1', null, null, '0');
INSERT INTO `core_reservation_storage` VALUES ('6', '1', '2', null, null, '0');
INSERT INTO `core_reservation_storage` VALUES ('7', '1', '3', null, null, '0');
INSERT INTO `core_reservation_storage` VALUES ('8', '1', '4', null, null, '0');
INSERT INTO `core_reservation_storage` VALUES ('9', '2', '1', '2013-12-01 17:28:48', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('10', '2', '2', '2013-12-01 17:28:52', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('12', '2', '3', '2013-12-01 17:28:56', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('14', '2', '4', '2013-12-01 17:29:00', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('15', '3', '5', '2013-12-01 18:23:54', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('16', '3', '6', '2013-12-01 18:24:00', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('17', '3', '7', '2013-12-01 18:24:06', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('18', '3', '8', '2013-12-01 18:24:12', null, '0');
INSERT INTO `core_reservation_storage` VALUES ('19', '3', '9', '2013-12-01 18:24:17', null, '0');

-- ----------------------------
-- Table structure for core_storage
-- ----------------------------
DROP TABLE IF EXISTS `core_storage`;
CREATE TABLE `core_storage` (
  `storage_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chamber_id` bigint(20) unsigned NOT NULL,
  `name` varchar(150) NOT NULL,
  `type` enum('slot','tray') NOT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive/reserved, 1 = active/available',
  PRIMARY KEY (`storage_id`),
  UNIQUE KEY `ndx_chamber_id_name` (`chamber_id`,`name`) USING BTREE,
  KEY `ndx_chamber_id` (`chamber_id`) USING BTREE,
  CONSTRAINT `fk_cs_chamber_id` FOREIGN KEY (`chamber_id`) REFERENCES `core_chamber` (`chamber_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1542 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_storage
-- ----------------------------
INSERT INTO `core_storage` VALUES ('1', '1', 'Storage 1', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('2', '1', 'Storage 2', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('3', '1', 'Storage 3', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('4', '1', 'Storage 4', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('5', '1', 'Storage 5', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('6', '1', 'Storage 6', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('7', '1', 'Storage 7', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('8', '1', 'Storage 8', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('9', '1', 'Storage 9', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('10', '1', 'Storage 10', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('11', '1', 'Storage 11', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('12', '1', 'Storage 12', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('13', '1', 'Storage 13', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('14', '1', 'Storage 14', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('15', '1', 'Storage 15', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('16', '1', 'Storage 16', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('17', '1', 'Storage 17', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('18', '1', 'Storage 18', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('19', '1', 'Storage 19', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('20', '1', 'Storage 20', 'slot', null, null, '1');
INSERT INTO `core_storage` VALUES ('21', '2', 'Storage 1', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('22', '2', 'Storage 2', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('23', '2', 'Storage 3', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('24', '2', 'Storage 4', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('25', '2', 'Storage 5', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('26', '2', 'Storage 6', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('27', '2', 'Storage 7', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('28', '2', 'Storage 8', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('29', '2', 'Storage 9', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('30', '2', 'Storage 10', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('31', '2', 'Storage 11', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('32', '2', 'Storage 12', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('33', '2', 'Storage 13', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('34', '2', 'Storage 14', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('35', '2', 'Storage 15', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('36', '2', 'Storage 16', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('37', '2', 'Storage 17', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('38', '2', 'Storage 18', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('39', '2', 'Storage 19', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('40', '2', 'Storage 20', 'slot', null, '2013-12-01 16:29:27', '1');
INSERT INTO `core_storage` VALUES ('41', '4', 'Storage 1', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('42', '4', 'Storage 2', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('43', '4', 'Storage 3', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('44', '4', 'Storage 4', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('45', '4', 'Storage 5', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('46', '4', 'Storage 6', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('47', '4', 'Storage 7', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('48', '4', 'Storage 8', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('49', '4', 'Storage 9', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('50', '4', 'Storage 10', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('51', '4', 'Storage 11', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('52', '4', 'Storage 12', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('53', '4', 'Storage 13', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('54', '4', 'Storage 14', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('55', '4', 'Storage 15', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('56', '4', 'Storage 16', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('57', '4', 'Storage 17', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('58', '4', 'Storage 18', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('59', '4', 'Storage 19', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('60', '4', 'Storage 20', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('61', '4', 'Storage 21', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('62', '4', 'Storage 22', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('63', '4', 'Storage 23', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('64', '4', 'Storage 24', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('65', '4', 'Storage 25', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('66', '4', 'Storage 26', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('67', '4', 'Storage 27', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('68', '4', 'Storage 28', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('69', '4', 'Storage 29', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('70', '4', 'Storage 30', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('71', '4', 'Storage 31', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('72', '4', 'Storage 32', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('73', '4', 'Storage 33', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('74', '4', 'Storage 34', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('75', '4', 'Storage 35', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('76', '4', 'Storage 36', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('77', '4', 'Storage 37', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('78', '4', 'Storage 38', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('79', '4', 'Storage 39', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('80', '4', 'Storage 40', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('81', '4', 'Storage 41', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('82', '4', 'Storage 42', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('83', '4', 'Storage 43', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('84', '4', 'Storage 44', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('85', '4', 'Storage 45', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('86', '4', 'Storage 46', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('87', '4', 'Storage 47', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('88', '4', 'Storage 48', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('89', '4', 'Storage 49', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('90', '4', 'Storage 50', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('91', '4', 'Storage 51', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('92', '5', 'Storage 1', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('93', '5', 'Storage 2', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('94', '5', 'Storage 3', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('95', '5', 'Storage 4', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('96', '5', 'Storage 5', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('97', '5', 'Storage 6', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('98', '5', 'Storage 7', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('99', '5', 'Storage 8', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('100', '5', 'Storage 9', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('101', '5', 'Storage 10', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('102', '5', 'Storage 11', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('103', '5', 'Storage 12', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('104', '5', 'Storage 13', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('105', '5', 'Storage 14', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('106', '5', 'Storage 15', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('107', '5', 'Storage 16', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('108', '5', 'Storage 17', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('109', '5', 'Storage 18', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('110', '5', 'Storage 19', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('111', '5', 'Storage 20', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('112', '5', 'Storage 21', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('113', '5', 'Storage 22', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('114', '5', 'Storage 23', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('115', '5', 'Storage 24', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('116', '5', 'Storage 25', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('117', '5', 'Storage 26', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('118', '5', 'Storage 27', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('119', '5', 'Storage 28', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('120', '5', 'Storage 29', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('121', '5', 'Storage 30', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('122', '5', 'Storage 31', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('123', '5', 'Storage 32', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('124', '5', 'Storage 33', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('125', '5', 'Storage 34', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('126', '5', 'Storage 35', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('127', '5', 'Storage 36', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('128', '5', 'Storage 37', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('129', '5', 'Storage 38', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('130', '5', 'Storage 39', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('131', '5', 'Storage 40', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('132', '5', 'Storage 41', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('133', '5', 'Storage 42', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('134', '5', 'Storage 43', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('135', '5', 'Storage 44', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('136', '5', 'Storage 45', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('137', '5', 'Storage 46', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('138', '5', 'Storage 47', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('139', '5', 'Storage 48', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('140', '5', 'Storage 49', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('141', '5', 'Storage 50', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('142', '5', 'Storage 51', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('143', '6', 'Storage 1', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('144', '6', 'Storage 2', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('145', '6', 'Storage 3', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('146', '6', 'Storage 4', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('147', '6', 'Storage 5', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('148', '6', 'Storage 6', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('149', '6', 'Storage 7', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('150', '6', 'Storage 8', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('151', '6', 'Storage 9', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('152', '6', 'Storage 10', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('153', '6', 'Storage 11', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('154', '6', 'Storage 12', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('155', '6', 'Storage 13', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('156', '6', 'Storage 14', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('157', '6', 'Storage 15', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('158', '6', 'Storage 16', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('159', '6', 'Storage 17', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('160', '6', 'Storage 18', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('161', '6', 'Storage 19', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('162', '6', 'Storage 20', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('163', '6', 'Storage 21', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('164', '6', 'Storage 22', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('165', '6', 'Storage 23', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('166', '6', 'Storage 24', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('167', '6', 'Storage 25', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('168', '6', 'Storage 26', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('169', '6', 'Storage 27', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('170', '6', 'Storage 28', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('171', '6', 'Storage 29', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('172', '6', 'Storage 30', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('173', '6', 'Storage 31', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('174', '6', 'Storage 32', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('175', '6', 'Storage 33', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('176', '6', 'Storage 34', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('177', '6', 'Storage 35', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('178', '6', 'Storage 36', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('179', '6', 'Storage 37', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('180', '6', 'Storage 38', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('181', '6', 'Storage 39', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('182', '6', 'Storage 40', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('183', '6', 'Storage 41', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('184', '6', 'Storage 42', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('185', '6', 'Storage 43', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('186', '6', 'Storage 44', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('187', '6', 'Storage 45', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('188', '6', 'Storage 46', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('189', '6', 'Storage 47', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('190', '6', 'Storage 48', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('191', '6', 'Storage 49', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('192', '6', 'Storage 50', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('193', '6', 'Storage 51', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('194', '7', 'Storage 1', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('195', '7', 'Storage 2', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('196', '7', 'Storage 3', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('197', '7', 'Storage 4', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('198', '7', 'Storage 5', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('199', '7', 'Storage 6', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('200', '7', 'Storage 7', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('201', '7', 'Storage 8', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('202', '7', 'Storage 9', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('203', '7', 'Storage 10', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('204', '7', 'Storage 11', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('205', '7', 'Storage 12', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('206', '7', 'Storage 13', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('207', '7', 'Storage 14', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('208', '7', 'Storage 15', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('209', '7', 'Storage 16', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('210', '7', 'Storage 17', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('211', '7', 'Storage 18', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('212', '7', 'Storage 19', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('213', '7', 'Storage 20', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('214', '7', 'Storage 21', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('215', '7', 'Storage 22', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('216', '7', 'Storage 23', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('217', '7', 'Storage 24', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('218', '7', 'Storage 25', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('219', '7', 'Storage 26', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('220', '7', 'Storage 27', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('221', '7', 'Storage 28', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('222', '7', 'Storage 29', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('223', '7', 'Storage 30', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('224', '7', 'Storage 31', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('225', '7', 'Storage 32', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('226', '7', 'Storage 33', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('227', '7', 'Storage 34', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('228', '7', 'Storage 35', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('229', '7', 'Storage 36', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('230', '7', 'Storage 37', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('231', '7', 'Storage 38', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('232', '7', 'Storage 39', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('233', '7', 'Storage 40', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('234', '7', 'Storage 41', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('235', '7', 'Storage 42', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('236', '7', 'Storage 43', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('237', '7', 'Storage 44', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('238', '7', 'Storage 45', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('239', '7', 'Storage 46', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('240', '7', 'Storage 47', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('241', '7', 'Storage 48', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('242', '7', 'Storage 49', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('243', '7', 'Storage 50', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('244', '7', 'Storage 51', 'slot', null, '2013-12-01 16:35:00', '1');
INSERT INTO `core_storage` VALUES ('245', '8', 'Storage 1', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('246', '8', 'Storage 2', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('247', '8', 'Storage 3', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('248', '8', 'Storage 4', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('249', '8', 'Storage 5', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('250', '8', 'Storage 6', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('251', '8', 'Storage 7', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('252', '8', 'Storage 8', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('253', '8', 'Storage 9', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('254', '8', 'Storage 10', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('255', '8', 'Storage 11', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('256', '8', 'Storage 12', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('257', '8', 'Storage 13', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('258', '8', 'Storage 14', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('259', '8', 'Storage 15', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('260', '8', 'Storage 16', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('261', '8', 'Storage 17', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('262', '8', 'Storage 18', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('263', '8', 'Storage 19', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('264', '8', 'Storage 20', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('265', '8', 'Storage 21', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('266', '8', 'Storage 22', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('267', '8', 'Storage 23', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('268', '8', 'Storage 24', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('269', '8', 'Storage 25', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('270', '8', 'Storage 26', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('271', '8', 'Storage 27', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('272', '8', 'Storage 28', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('273', '8', 'Storage 29', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('274', '8', 'Storage 30', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('275', '8', 'Storage 31', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('276', '8', 'Storage 32', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('277', '8', 'Storage 33', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('278', '8', 'Storage 34', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('279', '8', 'Storage 35', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('280', '8', 'Storage 36', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('281', '8', 'Storage 37', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('282', '8', 'Storage 38', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('283', '8', 'Storage 39', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('284', '8', 'Storage 40', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('285', '8', 'Storage 41', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('286', '8', 'Storage 42', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('287', '8', 'Storage 43', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('288', '8', 'Storage 44', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('289', '8', 'Storage 45', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('290', '8', 'Storage 46', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('291', '8', 'Storage 47', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('292', '8', 'Storage 48', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('293', '8', 'Storage 49', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('294', '8', 'Storage 50', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('295', '8', 'Storage 51', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('296', '8', 'Storage 52', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('297', '9', 'Storage 1', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('298', '9', 'Storage 2', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('299', '9', 'Storage 3', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('300', '9', 'Storage 4', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('301', '9', 'Storage 5', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('302', '9', 'Storage 6', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('303', '9', 'Storage 7', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('304', '9', 'Storage 8', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('305', '9', 'Storage 9', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('306', '9', 'Storage 10', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('307', '9', 'Storage 11', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('308', '9', 'Storage 12', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('309', '9', 'Storage 13', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('310', '9', 'Storage 14', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('311', '9', 'Storage 15', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('312', '9', 'Storage 16', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('313', '9', 'Storage 17', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('314', '9', 'Storage 18', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('315', '9', 'Storage 19', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('316', '9', 'Storage 20', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('317', '9', 'Storage 21', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('318', '9', 'Storage 22', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('319', '9', 'Storage 23', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('320', '9', 'Storage 24', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('321', '9', 'Storage 25', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('322', '9', 'Storage 26', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('323', '9', 'Storage 27', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('324', '9', 'Storage 28', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('325', '9', 'Storage 29', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('326', '9', 'Storage 30', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('327', '9', 'Storage 31', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('328', '9', 'Storage 32', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('329', '9', 'Storage 33', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('330', '9', 'Storage 34', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('331', '9', 'Storage 35', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('332', '9', 'Storage 36', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('333', '9', 'Storage 37', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('334', '9', 'Storage 38', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('335', '9', 'Storage 39', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('336', '9', 'Storage 40', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('337', '9', 'Storage 41', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('338', '9', 'Storage 42', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('339', '9', 'Storage 43', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('340', '9', 'Storage 44', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('341', '9', 'Storage 45', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('342', '9', 'Storage 46', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('343', '9', 'Storage 47', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('344', '9', 'Storage 48', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('345', '9', 'Storage 49', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('346', '9', 'Storage 50', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('347', '9', 'Storage 51', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('348', '9', 'Storage 52', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('349', '10', 'Storage 1', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('350', '10', 'Storage 2', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('351', '10', 'Storage 3', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('352', '10', 'Storage 4', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('353', '10', 'Storage 5', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('354', '10', 'Storage 6', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('355', '10', 'Storage 7', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('356', '10', 'Storage 8', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('357', '10', 'Storage 9', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('358', '10', 'Storage 10', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('359', '10', 'Storage 11', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('360', '10', 'Storage 12', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('361', '10', 'Storage 13', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('362', '10', 'Storage 14', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('363', '10', 'Storage 15', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('364', '10', 'Storage 16', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('365', '10', 'Storage 17', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('366', '10', 'Storage 18', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('367', '10', 'Storage 19', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('368', '10', 'Storage 20', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('369', '10', 'Storage 21', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('370', '10', 'Storage 22', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('371', '10', 'Storage 23', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('372', '10', 'Storage 24', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('373', '10', 'Storage 25', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('374', '10', 'Storage 26', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('375', '10', 'Storage 27', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('376', '10', 'Storage 28', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('377', '10', 'Storage 29', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('378', '10', 'Storage 30', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('379', '10', 'Storage 31', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('380', '10', 'Storage 32', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('381', '10', 'Storage 33', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('382', '10', 'Storage 34', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('383', '10', 'Storage 35', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('384', '10', 'Storage 36', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('385', '10', 'Storage 37', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('386', '10', 'Storage 38', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('387', '10', 'Storage 39', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('388', '10', 'Storage 40', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('389', '10', 'Storage 41', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('390', '10', 'Storage 42', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('391', '10', 'Storage 43', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('392', '10', 'Storage 44', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('393', '10', 'Storage 45', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('394', '10', 'Storage 46', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('395', '10', 'Storage 47', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('396', '10', 'Storage 48', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('397', '10', 'Storage 49', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('398', '10', 'Storage 50', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('399', '10', 'Storage 51', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('400', '10', 'Storage 52', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('401', '11', 'Storage 1', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('402', '11', 'Storage 2', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('403', '11', 'Storage 3', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('404', '11', 'Storage 4', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('405', '11', 'Storage 5', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('406', '11', 'Storage 6', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('407', '11', 'Storage 7', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('408', '11', 'Storage 8', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('409', '11', 'Storage 9', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('410', '11', 'Storage 10', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('411', '11', 'Storage 11', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('412', '11', 'Storage 12', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('413', '11', 'Storage 13', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('414', '11', 'Storage 14', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('415', '11', 'Storage 15', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('416', '11', 'Storage 16', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('417', '11', 'Storage 17', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('418', '11', 'Storage 18', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('419', '11', 'Storage 19', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('420', '11', 'Storage 20', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('421', '11', 'Storage 21', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('422', '11', 'Storage 22', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('423', '11', 'Storage 23', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('424', '11', 'Storage 24', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('425', '11', 'Storage 25', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('426', '11', 'Storage 26', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('427', '11', 'Storage 27', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('428', '11', 'Storage 28', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('429', '11', 'Storage 29', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('430', '11', 'Storage 30', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('431', '11', 'Storage 31', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('432', '11', 'Storage 32', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('433', '11', 'Storage 33', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('434', '11', 'Storage 34', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('435', '11', 'Storage 35', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('436', '11', 'Storage 36', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('437', '11', 'Storage 37', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('438', '11', 'Storage 38', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('439', '11', 'Storage 39', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('440', '11', 'Storage 40', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('441', '11', 'Storage 41', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('442', '11', 'Storage 42', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('443', '11', 'Storage 43', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('444', '11', 'Storage 44', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('445', '11', 'Storage 45', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('446', '11', 'Storage 46', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('447', '11', 'Storage 47', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('448', '11', 'Storage 48', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('449', '11', 'Storage 49', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('450', '11', 'Storage 50', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('451', '11', 'Storage 51', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('452', '11', 'Storage 52', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('453', '12', 'Storage 1', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('454', '12', 'Storage 2', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('455', '12', 'Storage 3', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('456', '12', 'Storage 4', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('457', '12', 'Storage 5', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('458', '12', 'Storage 6', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('459', '12', 'Storage 7', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('460', '12', 'Storage 8', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('461', '12', 'Storage 9', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('462', '12', 'Storage 10', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('463', '12', 'Storage 11', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('464', '12', 'Storage 12', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('465', '12', 'Storage 13', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('466', '12', 'Storage 14', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('467', '12', 'Storage 15', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('468', '12', 'Storage 16', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('469', '12', 'Storage 17', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('470', '12', 'Storage 18', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('471', '12', 'Storage 19', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('472', '12', 'Storage 20', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('473', '12', 'Storage 21', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('474', '12', 'Storage 22', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('475', '12', 'Storage 23', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('476', '12', 'Storage 24', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('477', '12', 'Storage 25', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('478', '12', 'Storage 26', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('479', '12', 'Storage 27', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('480', '12', 'Storage 28', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('481', '12', 'Storage 29', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('482', '12', 'Storage 30', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('483', '12', 'Storage 31', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('484', '12', 'Storage 32', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('485', '12', 'Storage 33', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('486', '12', 'Storage 34', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('487', '12', 'Storage 35', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('488', '12', 'Storage 36', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('489', '12', 'Storage 37', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('490', '12', 'Storage 38', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('491', '12', 'Storage 39', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('492', '12', 'Storage 40', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('493', '12', 'Storage 41', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('494', '12', 'Storage 42', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('495', '12', 'Storage 43', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('496', '12', 'Storage 44', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('497', '12', 'Storage 45', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('498', '12', 'Storage 46', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('499', '12', 'Storage 47', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('500', '12', 'Storage 48', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('501', '12', 'Storage 49', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('502', '12', 'Storage 50', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('503', '12', 'Storage 51', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('504', '12', 'Storage 52', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('505', '13', 'Storage 1', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('506', '13', 'Storage 2', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('507', '13', 'Storage 3', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('508', '13', 'Storage 4', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('509', '13', 'Storage 5', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('510', '13', 'Storage 6', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('511', '13', 'Storage 7', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('512', '13', 'Storage 8', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('513', '13', 'Storage 9', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('514', '13', 'Storage 10', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('515', '13', 'Storage 11', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('516', '13', 'Storage 12', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('517', '13', 'Storage 13', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('518', '13', 'Storage 14', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('519', '13', 'Storage 15', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('520', '13', 'Storage 16', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('521', '13', 'Storage 17', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('522', '13', 'Storage 18', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('523', '13', 'Storage 19', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('524', '13', 'Storage 20', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('525', '13', 'Storage 21', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('526', '13', 'Storage 22', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('527', '13', 'Storage 23', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('528', '13', 'Storage 24', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('529', '13', 'Storage 25', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('530', '13', 'Storage 26', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('531', '13', 'Storage 27', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('532', '13', 'Storage 28', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('533', '13', 'Storage 29', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('534', '13', 'Storage 30', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('535', '13', 'Storage 31', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('536', '13', 'Storage 32', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('537', '13', 'Storage 33', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('538', '13', 'Storage 34', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('539', '13', 'Storage 35', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('540', '13', 'Storage 36', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('541', '13', 'Storage 37', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('542', '13', 'Storage 38', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('543', '13', 'Storage 39', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('544', '13', 'Storage 40', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('545', '13', 'Storage 41', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('546', '13', 'Storage 42', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('547', '13', 'Storage 43', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('548', '13', 'Storage 44', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('549', '13', 'Storage 45', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('550', '13', 'Storage 46', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('551', '13', 'Storage 47', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('552', '13', 'Storage 48', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('553', '13', 'Storage 49', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('554', '13', 'Storage 50', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('555', '13', 'Storage 51', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('556', '13', 'Storage 52', 'slot', null, '2013-12-01 16:41:31', '1');
INSERT INTO `core_storage` VALUES ('557', '16', 'Storage 1', 'slot', null, '2013-12-01 16:45:08', '1');
INSERT INTO `core_storage` VALUES ('558', '16', 'Storage 2', 'slot', null, '2013-12-01 16:45:08', '1');
INSERT INTO `core_storage` VALUES ('559', '16', 'Storage 3', 'slot', null, '2013-12-01 16:45:08', '1');
INSERT INTO `core_storage` VALUES ('560', '16', 'Storage 4', 'slot', null, '2013-12-01 16:45:08', '1');
INSERT INTO `core_storage` VALUES ('561', '16', 'Storage 5', 'slot', null, '2013-12-01 16:45:08', '1');
INSERT INTO `core_storage` VALUES ('562', '17', 'Storage 1', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('563', '17', 'Storage 2', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('564', '17', 'Storage 3', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('565', '17', 'Storage 4', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('566', '17', 'Storage 5', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('567', '17', 'Storage 6', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('568', '17', 'Storage 7', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('569', '17', 'Storage 8', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('570', '17', 'Storage 9', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('571', '17', 'Storage 10', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('572', '18', 'Storage 1', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('573', '18', 'Storage 2', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('574', '18', 'Storage 3', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('575', '18', 'Storage 4', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('576', '18', 'Storage 5', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('577', '18', 'Storage 6', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('578', '18', 'Storage 7', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('579', '18', 'Storage 8', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('580', '18', 'Storage 9', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('581', '18', 'Storage 10', 'slot', null, '2013-12-01 16:48:07', '1');
INSERT INTO `core_storage` VALUES ('582', '19', 'Storage 1', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('583', '19', 'Storage 2', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('584', '19', 'Storage 3', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('585', '19', 'Storage 4', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('586', '19', 'Storage 5', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('587', '19', 'Storage 6', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('588', '19', 'Storage 7', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('589', '19', 'Storage 8', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('590', '19', 'Storage 9', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('591', '19', 'Storage 10', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('592', '19', 'Storage 11', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('593', '19', 'Storage 12', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('594', '19', 'Storage 13', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('595', '19', 'Storage 14', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('596', '19', 'Storage 15', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('597', '19', 'Storage 16', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('598', '19', 'Storage 17', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('599', '19', 'Storage 18', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('600', '19', 'Storage 19', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('601', '19', 'Storage 20', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('602', '19', 'Storage 21', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('603', '19', 'Storage 22', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('604', '19', 'Storage 23', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('605', '19', 'Storage 24', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('606', '19', 'Storage 25', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('607', '19', 'Storage 26', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('608', '19', 'Storage 27', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('609', '19', 'Storage 28', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('610', '19', 'Storage 29', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('611', '19', 'Storage 30', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('612', '19', 'Storage 31', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('613', '19', 'Storage 32', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('614', '19', 'Storage 33', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('615', '19', 'Storage 34', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('616', '19', 'Storage 35', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('617', '19', 'Storage 36', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('618', '19', 'Storage 37', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('619', '19', 'Storage 38', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('620', '19', 'Storage 39', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('621', '19', 'Storage 40', 'slot', null, '2013-12-01 16:51:17', '1');
INSERT INTO `core_storage` VALUES ('622', '20', 'Storage 1', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('623', '20', 'Storage 2', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('624', '20', 'Storage 3', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('625', '20', 'Storage 4', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('626', '20', 'Storage 5', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('627', '20', 'Storage 6', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('628', '20', 'Storage 7', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('629', '20', 'Storage 8', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('630', '20', 'Storage 9', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('631', '20', 'Storage 10', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('632', '20', 'Storage 11', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('633', '20', 'Storage 12', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('634', '20', 'Storage 13', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('635', '20', 'Storage 14', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('636', '20', 'Storage 15', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('637', '20', 'Storage 16', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('638', '20', 'Storage 17', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('639', '20', 'Storage 18', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('640', '20', 'Storage 19', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('641', '20', 'Storage 20', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('642', '20', 'Storage 21', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('643', '20', 'Storage 22', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('644', '20', 'Storage 23', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('645', '20', 'Storage 24', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('646', '20', 'Storage 25', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('647', '20', 'Storage 26', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('648', '20', 'Storage 27', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('649', '20', 'Storage 28', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('650', '20', 'Storage 29', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('651', '20', 'Storage 30', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('652', '20', 'Storage 31', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('653', '20', 'Storage 32', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('654', '20', 'Storage 33', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('655', '20', 'Storage 34', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('656', '20', 'Storage 35', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('657', '20', 'Storage 36', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('658', '20', 'Storage 37', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('659', '20', 'Storage 38', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('660', '20', 'Storage 39', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('661', '20', 'Storage 40', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('662', '21', 'Storage 1', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('663', '21', 'Storage 2', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('664', '21', 'Storage 3', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('665', '21', 'Storage 4', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('666', '21', 'Storage 5', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('667', '21', 'Storage 6', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('668', '21', 'Storage 7', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('669', '21', 'Storage 8', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('670', '21', 'Storage 9', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('671', '21', 'Storage 10', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('672', '21', 'Storage 11', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('673', '21', 'Storage 12', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('674', '21', 'Storage 13', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('675', '21', 'Storage 14', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('676', '21', 'Storage 15', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('677', '21', 'Storage 16', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('678', '21', 'Storage 17', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('679', '21', 'Storage 18', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('680', '21', 'Storage 19', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('681', '21', 'Storage 20', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('682', '21', 'Storage 21', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('683', '21', 'Storage 22', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('684', '21', 'Storage 23', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('685', '21', 'Storage 24', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('686', '21', 'Storage 25', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('687', '21', 'Storage 26', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('688', '21', 'Storage 27', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('689', '21', 'Storage 28', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('690', '21', 'Storage 29', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('691', '21', 'Storage 30', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('692', '21', 'Storage 31', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('693', '21', 'Storage 32', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('694', '21', 'Storage 33', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('695', '21', 'Storage 34', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('696', '21', 'Storage 35', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('697', '21', 'Storage 36', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('698', '21', 'Storage 37', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('699', '21', 'Storage 38', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('700', '21', 'Storage 39', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('701', '21', 'Storage 40', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('702', '22', 'Storage 1', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('703', '22', 'Storage 2', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('704', '22', 'Storage 3', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('705', '22', 'Storage 4', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('706', '22', 'Storage 5', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('707', '22', 'Storage 6', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('708', '22', 'Storage 7', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('709', '22', 'Storage 8', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('710', '22', 'Storage 9', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('711', '22', 'Storage 10', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('712', '22', 'Storage 11', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('713', '22', 'Storage 12', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('714', '22', 'Storage 13', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('715', '22', 'Storage 14', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('716', '22', 'Storage 15', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('717', '22', 'Storage 16', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('718', '22', 'Storage 17', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('719', '22', 'Storage 18', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('720', '22', 'Storage 19', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('721', '22', 'Storage 20', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('722', '22', 'Storage 21', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('723', '22', 'Storage 22', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('724', '22', 'Storage 23', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('725', '22', 'Storage 24', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('726', '22', 'Storage 25', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('727', '22', 'Storage 26', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('728', '22', 'Storage 27', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('729', '22', 'Storage 28', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('730', '22', 'Storage 29', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('731', '22', 'Storage 30', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('732', '22', 'Storage 31', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('733', '22', 'Storage 32', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('734', '22', 'Storage 33', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('735', '22', 'Storage 34', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('736', '22', 'Storage 35', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('737', '22', 'Storage 36', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('738', '22', 'Storage 37', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('739', '22', 'Storage 38', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('740', '22', 'Storage 39', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('741', '22', 'Storage 40', 'slot', null, '2013-12-01 16:53:50', '1');
INSERT INTO `core_storage` VALUES ('742', '23', 'Storage 1', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('743', '23', 'Storage 2', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('744', '23', 'Storage 3', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('745', '23', 'Storage 4', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('746', '23', 'Storage 5', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('747', '23', 'Storage 6', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('748', '23', 'Storage 7', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('749', '23', 'Storage 8', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('750', '23', 'Storage 9', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('751', '23', 'Storage 10', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('752', '23', 'Storage 11', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('753', '23', 'Storage 12', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('754', '23', 'Storage 13', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('755', '23', 'Storage 14', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('756', '23', 'Storage 15', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('757', '23', 'Storage 16', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('758', '23', 'Storage 17', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('759', '23', 'Storage 18', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('760', '23', 'Storage 19', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('761', '23', 'Storage 20', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('762', '23', 'Storage 21', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('763', '23', 'Storage 22', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('764', '23', 'Storage 23', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('765', '23', 'Storage 24', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('766', '23', 'Storage 25', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('767', '23', 'Storage 26', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('768', '23', 'Storage 27', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('769', '23', 'Storage 28', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('770', '23', 'Storage 29', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('771', '23', 'Storage 30', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('772', '23', 'Storage 31', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('773', '23', 'Storage 32', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('774', '23', 'Storage 33', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('775', '23', 'Storage 34', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('776', '23', 'Storage 35', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('777', '23', 'Storage 36', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('778', '23', 'Storage 37', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('779', '23', 'Storage 38', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('780', '23', 'Storage 39', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('781', '23', 'Storage 40', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('782', '25', 'Storage 1', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('783', '25', 'Storage 2', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('784', '25', 'Storage 3', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('785', '25', 'Storage 4', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('786', '25', 'Storage 5', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('787', '25', 'Storage 6', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('788', '25', 'Storage 7', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('789', '25', 'Storage 8', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('790', '25', 'Storage 9', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('791', '25', 'Storage 10', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('792', '25', 'Storage 11', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('793', '25', 'Storage 12', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('794', '25', 'Storage 13', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('795', '25', 'Storage 14', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('796', '25', 'Storage 15', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('797', '25', 'Storage 16', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('798', '25', 'Storage 17', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('799', '25', 'Storage 18', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('800', '25', 'Storage 19', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('801', '25', 'Storage 20', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('802', '25', 'Storage 21', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('803', '25', 'Storage 22', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('804', '25', 'Storage 23', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('805', '25', 'Storage 24', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('806', '25', 'Storage 25', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('807', '25', 'Storage 26', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('808', '25', 'Storage 27', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('809', '25', 'Storage 28', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('810', '25', 'Storage 29', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('811', '25', 'Storage 30', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('812', '25', 'Storage 31', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('813', '25', 'Storage 32', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('814', '25', 'Storage 33', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('815', '25', 'Storage 34', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('816', '25', 'Storage 35', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('817', '25', 'Storage 36', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('818', '25', 'Storage 37', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('819', '25', 'Storage 38', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('820', '25', 'Storage 39', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('821', '25', 'Storage 40', 'slot', null, '2013-12-01 17:04:52', '1');
INSERT INTO `core_storage` VALUES ('822', '27', 'Storage 1', 'slot', null, '2013-12-01 17:06:43', '1');
INSERT INTO `core_storage` VALUES ('823', '27', 'Storage 2', 'slot', null, '2013-12-01 17:06:43', '1');
INSERT INTO `core_storage` VALUES ('824', '28', 'Storage 1', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('825', '28', 'Storage 2', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('826', '28', 'Storage 3', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('827', '28', 'Storage 4', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('828', '28', 'Storage 5', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('829', '28', 'Storage 6', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('830', '28', 'Storage 7', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('831', '28', 'Storage 8', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('832', '28', 'Storage 9', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('833', '28', 'Storage 10', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('834', '28', 'Storage 11', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('835', '28', 'Storage 12', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('836', '28', 'Storage 13', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('837', '28', 'Storage 14', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('838', '28', 'Storage 15', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('839', '28', 'Storage 16', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('840', '28', 'Storage 17', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('841', '28', 'Storage 18', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('842', '28', 'Storage 19', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('843', '28', 'Storage 20', 'slot', null, '2013-12-01 17:08:32', '1');
INSERT INTO `core_storage` VALUES ('844', '29', 'Storage 1', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('845', '29', 'Storage 2', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('846', '29', 'Storage 3', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('847', '29', 'Storage 4', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('848', '29', 'Storage 5', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('849', '29', 'Storage 6', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('850', '29', 'Storage 7', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('851', '29', 'Storage 8', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('852', '29', 'Storage 9', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('853', '29', 'Storage 10', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('854', '29', 'Storage 11', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('855', '29', 'Storage 12', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('856', '29', 'Storage 13', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('857', '29', 'Storage 14', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('858', '29', 'Storage 15', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('859', '29', 'Storage 16', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('860', '29', 'Storage 17', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('861', '29', 'Storage 18', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('862', '29', 'Storage 19', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('863', '29', 'Storage 20', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('864', '29', 'Storage 21', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('865', '29', 'Storage 22', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('866', '29', 'Storage 23', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('867', '29', 'Storage 24', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('868', '29', 'Storage 25', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('869', '29', 'Storage 26', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('870', '29', 'Storage 27', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('871', '29', 'Storage 28', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('872', '29', 'Storage 29', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('873', '29', 'Storage 30', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('874', '29', 'Storage 31', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('875', '29', 'Storage 32', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('876', '29', 'Storage 33', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('877', '29', 'Storage 34', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('878', '29', 'Storage 35', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('879', '29', 'Storage 36', 'slot', null, '2013-12-01 17:14:46', '1');
INSERT INTO `core_storage` VALUES ('880', '30', 'Storage 1', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('881', '30', 'Storage 2', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('882', '30', 'Storage 3', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('883', '30', 'Storage 4', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('884', '30', 'Storage 5', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('885', '30', 'Storage 6', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('886', '30', 'Storage 7', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('887', '30', 'Storage 8', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('888', '30', 'Storage 9', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('889', '30', 'Storage 10', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('890', '30', 'Storage 11', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('891', '30', 'Storage 12', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('892', '30', 'Storage 13', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('893', '30', 'Storage 14', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('894', '30', 'Storage 15', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('895', '30', 'Storage 16', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('896', '30', 'Storage 17', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('897', '30', 'Storage 18', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('898', '30', 'Storage 19', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('899', '30', 'Storage 20', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('900', '30', 'Storage 21', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('901', '30', 'Storage 22', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('902', '30', 'Storage 23', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('903', '30', 'Storage 24', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('904', '30', 'Storage 25', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('905', '30', 'Storage 26', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('906', '30', 'Storage 27', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('907', '30', 'Storage 28', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('908', '30', 'Storage 29', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('909', '30', 'Storage 30', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('910', '30', 'Storage 31', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('911', '30', 'Storage 32', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('912', '30', 'Storage 33', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('913', '30', 'Storage 34', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('914', '30', 'Storage 35', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('915', '30', 'Storage 36', 'slot', null, '2013-12-01 17:14:57', '1');
INSERT INTO `core_storage` VALUES ('916', '31', 'Storage 1', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('917', '31', 'Storage 2', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('918', '31', 'Storage 3', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('919', '31', 'Storage 4', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('920', '31', 'Storage 5', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('921', '31', 'Storage 6', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('922', '31', 'Storage 7', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('923', '31', 'Storage 8', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('924', '31', 'Storage 9', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('925', '31', 'Storage 10', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('926', '31', 'Storage 11', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('927', '31', 'Storage 12', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('928', '31', 'Storage 13', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('929', '31', 'Storage 14', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('930', '31', 'Storage 15', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('931', '31', 'Storage 16', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('932', '31', 'Storage 17', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('933', '31', 'Storage 18', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('934', '31', 'Storage 19', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('935', '31', 'Storage 20', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('936', '31', 'Storage 21', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('937', '31', 'Storage 22', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('938', '31', 'Storage 23', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('939', '31', 'Storage 24', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('940', '31', 'Storage 25', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('941', '31', 'Storage 26', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('942', '31', 'Storage 27', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('943', '31', 'Storage 28', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('944', '31', 'Storage 29', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('945', '31', 'Storage 30', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('946', '31', 'Storage 31', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('947', '31', 'Storage 32', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('948', '31', 'Storage 33', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('949', '31', 'Storage 34', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('950', '31', 'Storage 35', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('951', '31', 'Storage 36', 'slot', null, '2013-12-01 17:15:02', '1');
INSERT INTO `core_storage` VALUES ('952', '32', 'Storage 1', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('953', '32', 'Storage 2', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('954', '32', 'Storage 3', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('955', '32', 'Storage 4', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('956', '32', 'Storage 5', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('957', '32', 'Storage 6', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('958', '32', 'Storage 7', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('959', '32', 'Storage 8', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('960', '32', 'Storage 9', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('961', '32', 'Storage 10', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('962', '32', 'Storage 11', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('963', '32', 'Storage 12', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('964', '32', 'Storage 13', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('965', '32', 'Storage 14', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('966', '32', 'Storage 15', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('967', '32', 'Storage 16', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('968', '32', 'Storage 17', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('969', '32', 'Storage 18', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('970', '32', 'Storage 19', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('971', '32', 'Storage 20', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('972', '32', 'Storage 21', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('973', '32', 'Storage 22', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('974', '32', 'Storage 23', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('975', '32', 'Storage 24', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('976', '32', 'Storage 25', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('977', '32', 'Storage 26', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('978', '32', 'Storage 27', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('979', '32', 'Storage 28', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('980', '32', 'Storage 29', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('981', '32', 'Storage 30', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('982', '32', 'Storage 31', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('983', '32', 'Storage 32', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('984', '32', 'Storage 33', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('985', '32', 'Storage 34', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('986', '32', 'Storage 35', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('987', '32', 'Storage 36', 'slot', null, '2013-12-01 17:15:06', '1');
INSERT INTO `core_storage` VALUES ('988', '33', 'Storage 1', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('989', '33', 'Storage 2', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('990', '33', 'Storage 3', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('991', '33', 'Storage 4', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('992', '33', 'Storage 5', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('993', '33', 'Storage 6', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('994', '33', 'Storage 7', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('995', '33', 'Storage 8', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('996', '33', 'Storage 9', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('997', '33', 'Storage 10', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('998', '33', 'Storage 11', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('999', '33', 'Storage 12', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1000', '33', 'Storage 13', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1001', '33', 'Storage 14', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1002', '33', 'Storage 15', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1003', '33', 'Storage 16', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1004', '33', 'Storage 17', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1005', '33', 'Storage 18', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1006', '33', 'Storage 19', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1007', '33', 'Storage 20', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1008', '33', 'Storage 21', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1009', '33', 'Storage 22', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1010', '33', 'Storage 23', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1011', '33', 'Storage 24', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1012', '33', 'Storage 25', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1013', '33', 'Storage 26', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1014', '33', 'Storage 27', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1015', '33', 'Storage 28', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1016', '33', 'Storage 29', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1017', '33', 'Storage 30', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1018', '33', 'Storage 31', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1019', '33', 'Storage 32', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1020', '33', 'Storage 33', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1021', '33', 'Storage 34', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1022', '33', 'Storage 35', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1023', '33', 'Storage 36', 'slot', null, '2013-12-01 17:15:10', '1');
INSERT INTO `core_storage` VALUES ('1024', '34', 'Storage 1', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1025', '34', 'Storage 2', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1026', '34', 'Storage 3', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1027', '34', 'Storage 4', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1028', '34', 'Storage 5', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1029', '34', 'Storage 6', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1030', '34', 'Storage 7', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1031', '34', 'Storage 8', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1032', '34', 'Storage 9', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1033', '34', 'Storage 10', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1034', '34', 'Storage 11', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1035', '34', 'Storage 12', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1036', '34', 'Storage 13', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1037', '34', 'Storage 14', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1038', '34', 'Storage 15', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1039', '34', 'Storage 16', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1040', '34', 'Storage 17', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1041', '34', 'Storage 18', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1042', '34', 'Storage 19', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1043', '34', 'Storage 20', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1044', '34', 'Storage 21', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1045', '34', 'Storage 22', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1046', '34', 'Storage 23', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1047', '34', 'Storage 24', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1048', '34', 'Storage 25', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1049', '34', 'Storage 26', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1050', '34', 'Storage 27', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1051', '34', 'Storage 28', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1052', '34', 'Storage 29', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1053', '34', 'Storage 30', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1054', '34', 'Storage 31', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1055', '34', 'Storage 32', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1056', '34', 'Storage 33', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1057', '34', 'Storage 34', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1058', '34', 'Storage 35', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1059', '34', 'Storage 36', 'slot', null, '2013-12-01 17:15:13', '1');
INSERT INTO `core_storage` VALUES ('1060', '35', 'Storage 1', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1061', '35', 'Storage 2', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1062', '35', 'Storage 3', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1063', '35', 'Storage 4', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1064', '35', 'Storage 5', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1065', '35', 'Storage 6', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1066', '35', 'Storage 7', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1067', '35', 'Storage 8', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1068', '35', 'Storage 9', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1069', '35', 'Storage 10', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1070', '35', 'Storage 11', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1071', '35', 'Storage 12', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1072', '35', 'Storage 13', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1073', '35', 'Storage 14', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1074', '35', 'Storage 15', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1075', '35', 'Storage 16', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1076', '35', 'Storage 17', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1077', '35', 'Storage 18', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1078', '35', 'Storage 19', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1079', '35', 'Storage 20', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1080', '35', 'Storage 21', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1081', '35', 'Storage 22', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1082', '35', 'Storage 23', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1083', '35', 'Storage 24', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1084', '35', 'Storage 25', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1085', '35', 'Storage 26', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1086', '35', 'Storage 27', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1087', '35', 'Storage 28', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1088', '35', 'Storage 29', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1089', '35', 'Storage 30', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1090', '35', 'Storage 31', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1091', '35', 'Storage 32', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1092', '35', 'Storage 33', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1093', '35', 'Storage 34', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1094', '35', 'Storage 35', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1095', '35', 'Storage 36', 'slot', null, '2013-12-01 17:15:17', '1');
INSERT INTO `core_storage` VALUES ('1096', '36', 'Storage 1', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1097', '36', 'Storage 2', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1098', '36', 'Storage 3', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1099', '36', 'Storage 4', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1100', '36', 'Storage 5', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1101', '36', 'Storage 6', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1102', '36', 'Storage 7', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1103', '36', 'Storage 8', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1104', '36', 'Storage 9', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1105', '36', 'Storage 10', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1106', '36', 'Storage 11', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1107', '36', 'Storage 12', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1108', '36', 'Storage 13', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1109', '36', 'Storage 14', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1110', '36', 'Storage 15', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1111', '36', 'Storage 16', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1112', '36', 'Storage 17', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1113', '36', 'Storage 18', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1114', '36', 'Storage 19', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1115', '36', 'Storage 20', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1116', '36', 'Storage 21', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1117', '36', 'Storage 22', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1118', '36', 'Storage 23', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1119', '36', 'Storage 24', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1120', '36', 'Storage 25', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1121', '36', 'Storage 26', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1122', '36', 'Storage 27', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1123', '36', 'Storage 28', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1124', '36', 'Storage 29', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1125', '36', 'Storage 30', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1126', '36', 'Storage 31', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1127', '36', 'Storage 32', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1128', '36', 'Storage 33', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1129', '36', 'Storage 34', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1130', '36', 'Storage 35', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1131', '36', 'Storage 36', 'slot', null, '2013-12-01 17:15:21', '1');
INSERT INTO `core_storage` VALUES ('1132', '37', 'Storage 1', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1133', '37', 'Storage 2', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1134', '37', 'Storage 3', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1135', '37', 'Storage 4', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1136', '37', 'Storage 5', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1137', '37', 'Storage 6', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1138', '37', 'Storage 7', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1139', '37', 'Storage 8', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1140', '37', 'Storage 9', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1141', '37', 'Storage 10', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1142', '37', 'Storage 11', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1143', '37', 'Storage 12', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1144', '37', 'Storage 13', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1145', '37', 'Storage 14', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1146', '37', 'Storage 15', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1147', '37', 'Storage 16', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1148', '37', 'Storage 17', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1149', '37', 'Storage 18', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1150', '37', 'Storage 19', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1151', '37', 'Storage 20', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1152', '37', 'Storage 21', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1153', '37', 'Storage 22', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1154', '37', 'Storage 23', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1155', '37', 'Storage 24', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1156', '37', 'Storage 25', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1157', '37', 'Storage 26', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1158', '37', 'Storage 27', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1159', '37', 'Storage 28', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1160', '37', 'Storage 29', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1161', '37', 'Storage 30', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1162', '37', 'Storage 31', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1163', '37', 'Storage 32', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1164', '37', 'Storage 33', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1165', '37', 'Storage 34', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1166', '37', 'Storage 35', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1167', '37', 'Storage 36', 'slot', null, '2013-12-01 17:15:24', '1');
INSERT INTO `core_storage` VALUES ('1168', '38', 'Storage 1', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1169', '38', 'Storage 2', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1170', '38', 'Storage 3', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1171', '38', 'Storage 4', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1172', '38', 'Storage 5', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1173', '38', 'Storage 6', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1174', '38', 'Storage 7', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1175', '38', 'Storage 8', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1176', '38', 'Storage 9', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1177', '38', 'Storage 10', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1178', '38', 'Storage 11', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1179', '38', 'Storage 12', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1180', '38', 'Storage 13', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1181', '38', 'Storage 14', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1182', '38', 'Storage 15', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1183', '38', 'Storage 16', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1184', '38', 'Storage 17', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1185', '38', 'Storage 18', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1186', '38', 'Storage 19', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1187', '38', 'Storage 20', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1188', '38', 'Storage 21', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1189', '38', 'Storage 22', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1190', '38', 'Storage 23', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1191', '38', 'Storage 24', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1192', '38', 'Storage 25', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1193', '38', 'Storage 26', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1194', '38', 'Storage 27', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1195', '38', 'Storage 28', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1196', '38', 'Storage 29', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1197', '38', 'Storage 30', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1198', '38', 'Storage 31', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1199', '38', 'Storage 32', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1200', '38', 'Storage 33', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1201', '38', 'Storage 34', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1202', '38', 'Storage 35', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1203', '38', 'Storage 36', 'slot', null, '2013-12-01 17:15:30', '1');
INSERT INTO `core_storage` VALUES ('1204', '39', 'Storage 1', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1205', '39', 'Storage 2', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1206', '39', 'Storage 3', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1207', '39', 'Storage 4', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1208', '39', 'Storage 5', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1209', '39', 'Storage 6', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1210', '39', 'Storage 7', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1211', '39', 'Storage 8', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1212', '39', 'Storage 9', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1213', '39', 'Storage 10', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1214', '39', 'Storage 11', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1215', '39', 'Storage 12', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1216', '39', 'Storage 13', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1217', '39', 'Storage 14', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1218', '39', 'Storage 15', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1219', '39', 'Storage 16', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1220', '39', 'Storage 17', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1221', '39', 'Storage 18', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1222', '39', 'Storage 19', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1223', '39', 'Storage 20', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1224', '39', 'Storage 21', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1225', '39', 'Storage 22', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1226', '39', 'Storage 23', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1227', '39', 'Storage 24', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1228', '39', 'Storage 25', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1229', '39', 'Storage 26', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1230', '39', 'Storage 27', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1231', '39', 'Storage 28', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1232', '39', 'Storage 29', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1233', '39', 'Storage 30', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1234', '39', 'Storage 31', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1235', '39', 'Storage 32', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1236', '39', 'Storage 33', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1237', '39', 'Storage 34', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1238', '39', 'Storage 35', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1239', '39', 'Storage 36', 'slot', null, '2013-12-01 17:15:34', '1');
INSERT INTO `core_storage` VALUES ('1240', '40', 'Storage 1', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1241', '40', 'Storage 2', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1242', '40', 'Storage 3', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1243', '40', 'Storage 4', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1244', '40', 'Storage 5', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1245', '40', 'Storage 6', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1246', '40', 'Storage 7', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1247', '40', 'Storage 8', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1248', '40', 'Storage 9', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1249', '40', 'Storage 10', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1250', '40', 'Storage 11', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1251', '40', 'Storage 12', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1252', '40', 'Storage 13', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1253', '40', 'Storage 14', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1254', '40', 'Storage 15', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1255', '40', 'Storage 16', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1256', '40', 'Storage 17', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1257', '40', 'Storage 18', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1258', '40', 'Storage 19', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1259', '40', 'Storage 20', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1260', '40', 'Storage 21', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1261', '40', 'Storage 22', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1262', '40', 'Storage 23', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1263', '40', 'Storage 24', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1264', '40', 'Storage 25', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1265', '40', 'Storage 26', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1266', '40', 'Storage 27', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1267', '40', 'Storage 28', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1268', '40', 'Storage 29', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1269', '40', 'Storage 30', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1270', '40', 'Storage 31', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1271', '40', 'Storage 32', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1272', '40', 'Storage 33', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1273', '40', 'Storage 34', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1274', '40', 'Storage 35', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1275', '40', 'Storage 36', 'slot', null, '2013-12-01 17:15:38', '1');
INSERT INTO `core_storage` VALUES ('1276', '41', 'Storage 1', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1277', '41', 'Storage 2', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1278', '41', 'Storage 3', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1279', '41', 'Storage 4', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1280', '41', 'Storage 5', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1281', '41', 'Storage 6', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1282', '41', 'Storage 7', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1283', '41', 'Storage 8', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1284', '41', 'Storage 9', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1285', '41', 'Storage 10', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1286', '41', 'Storage 11', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1287', '41', 'Storage 12', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1288', '41', 'Storage 13', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1289', '41', 'Storage 14', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1290', '41', 'Storage 15', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1291', '41', 'Storage 16', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1292', '41', 'Storage 17', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1293', '41', 'Storage 18', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1294', '41', 'Storage 19', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1295', '41', 'Storage 20', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1296', '41', 'Storage 21', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1297', '41', 'Storage 22', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1298', '41', 'Storage 23', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1299', '41', 'Storage 24', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1300', '41', 'Storage 25', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1301', '41', 'Storage 26', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1302', '41', 'Storage 27', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1303', '41', 'Storage 28', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1304', '41', 'Storage 29', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1305', '41', 'Storage 30', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1306', '41', 'Storage 31', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1307', '41', 'Storage 32', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1308', '41', 'Storage 33', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1309', '41', 'Storage 34', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1310', '41', 'Storage 35', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1311', '41', 'Storage 36', 'slot', null, '2013-12-01 17:15:41', '1');
INSERT INTO `core_storage` VALUES ('1312', '42', 'Storage 1', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1313', '42', 'Storage 2', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1314', '42', 'Storage 3', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1315', '42', 'Storage 4', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1316', '42', 'Storage 5', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1317', '42', 'Storage 6', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1318', '42', 'Storage 7', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1319', '42', 'Storage 8', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1320', '42', 'Storage 9', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1321', '42', 'Storage 10', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1322', '42', 'Storage 11', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1323', '42', 'Storage 12', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1324', '42', 'Storage 13', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1325', '42', 'Storage 14', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1326', '42', 'Storage 15', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1327', '42', 'Storage 16', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1328', '42', 'Storage 17', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1329', '42', 'Storage 18', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1330', '42', 'Storage 19', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1331', '42', 'Storage 20', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1332', '42', 'Storage 21', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1333', '42', 'Storage 22', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1334', '42', 'Storage 23', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1335', '42', 'Storage 24', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1336', '42', 'Storage 25', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1337', '42', 'Storage 26', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1338', '42', 'Storage 27', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1339', '42', 'Storage 28', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1340', '42', 'Storage 29', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1341', '42', 'Storage 30', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1342', '42', 'Storage 31', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1343', '42', 'Storage 32', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1344', '42', 'Storage 33', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1345', '42', 'Storage 34', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1346', '42', 'Storage 35', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1347', '42', 'Storage 36', 'slot', null, '2013-12-01 17:15:43', '1');
INSERT INTO `core_storage` VALUES ('1348', '43', 'Storage 1', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1349', '43', 'Storage 2', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1350', '43', 'Storage 3', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1351', '43', 'Storage 4', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1352', '43', 'Storage 5', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1353', '43', 'Storage 6', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1354', '43', 'Storage 7', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1355', '43', 'Storage 8', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1356', '43', 'Storage 9', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1357', '43', 'Storage 10', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1358', '43', 'Storage 11', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1359', '43', 'Storage 12', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1360', '43', 'Storage 13', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1361', '43', 'Storage 14', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1362', '43', 'Storage 15', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1363', '43', 'Storage 16', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1364', '43', 'Storage 17', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1365', '43', 'Storage 18', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1366', '43', 'Storage 19', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1367', '43', 'Storage 20', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1368', '43', 'Storage 21', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1369', '43', 'Storage 22', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1370', '43', 'Storage 23', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1371', '43', 'Storage 24', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1372', '43', 'Storage 25', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1373', '43', 'Storage 26', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1374', '43', 'Storage 27', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1375', '43', 'Storage 28', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1376', '43', 'Storage 29', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1377', '43', 'Storage 30', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1378', '43', 'Storage 31', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1379', '43', 'Storage 32', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1380', '43', 'Storage 33', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1381', '43', 'Storage 34', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1382', '43', 'Storage 35', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1383', '43', 'Storage 36', 'slot', null, '2013-12-01 17:15:47', '1');
INSERT INTO `core_storage` VALUES ('1384', '44', 'Storage 1', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1385', '44', 'Storage 2', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1386', '44', 'Storage 3', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1387', '44', 'Storage 4', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1388', '44', 'Storage 5', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1389', '44', 'Storage 6', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1390', '44', 'Storage 7', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1391', '44', 'Storage 8', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1392', '44', 'Storage 9', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1393', '44', 'Storage 10', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1394', '44', 'Storage 11', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1395', '44', 'Storage 12', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1396', '44', 'Storage 13', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1397', '44', 'Storage 14', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1398', '44', 'Storage 15', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1399', '44', 'Storage 16', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1400', '44', 'Storage 17', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1401', '44', 'Storage 18', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1402', '44', 'Storage 19', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1403', '44', 'Storage 20', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1404', '44', 'Storage 21', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1405', '44', 'Storage 22', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1406', '44', 'Storage 23', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1407', '44', 'Storage 24', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1408', '44', 'Storage 25', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1409', '44', 'Storage 26', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1410', '44', 'Storage 27', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1411', '44', 'Storage 28', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1412', '44', 'Storage 29', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1413', '44', 'Storage 30', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1414', '44', 'Storage 31', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1415', '44', 'Storage 32', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1416', '44', 'Storage 33', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1417', '44', 'Storage 34', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1418', '44', 'Storage 35', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1419', '44', 'Storage 36', 'slot', null, '2013-12-01 17:15:49', '1');
INSERT INTO `core_storage` VALUES ('1420', '45', 'Storage 1', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1421', '45', 'Storage 2', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1422', '45', 'Storage 3', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1423', '45', 'Storage 4', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1424', '45', 'Storage 5', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1425', '45', 'Storage 6', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1426', '45', 'Storage 7', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1427', '45', 'Storage 8', 'slot', null, '2013-12-01 17:18:35', '1');
INSERT INTO `core_storage` VALUES ('1428', '46', 'Storage 1', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1429', '46', 'Storage 2', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1430', '46', 'Storage 3', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1431', '46', 'Storage 4', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1432', '46', 'Storage 5', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1433', '46', 'Storage 6', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1434', '46', 'Storage 7', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1435', '46', 'Storage 8', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1436', '46', 'Storage 9', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1437', '46', 'Storage 10', 'slot', null, '2013-12-01 17:19:49', '1');
INSERT INTO `core_storage` VALUES ('1438', '47', 'Storage 1', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1439', '47', 'Storage 2', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1440', '47', 'Storage 3', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1441', '47', 'Storage 4', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1442', '47', 'Storage 5', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1443', '47', 'Storage 6', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1444', '47', 'Storage 7', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1445', '47', 'Storage 8', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1446', '47', 'Storage 9', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1447', '47', 'Storage 10', 'slot', null, '2013-12-01 17:19:55', '1');
INSERT INTO `core_storage` VALUES ('1448', '48', 'Storage 1', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1449', '48', 'Storage 2', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1450', '48', 'Storage 3', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1451', '48', 'Storage 4', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1452', '48', 'Storage 5', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1453', '48', 'Storage 6', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1454', '48', 'Storage 7', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1455', '48', 'Storage 8', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1456', '48', 'Storage 9', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1457', '48', 'Storage 10', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1458', '48', 'Storage 11', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1459', '48', 'Storage 12', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1460', '48', 'Storage 13', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1461', '48', 'Storage 14', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1462', '48', 'Storage 15', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1463', '48', 'Storage 16', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1464', '48', 'Storage 17', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1465', '48', 'Storage 18', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1466', '48', 'Storage 19', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1467', '48', 'Storage 20', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1468', '48', 'Storage 21', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1469', '48', 'Storage 22', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1470', '48', 'Storage 23', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1471', '48', 'Storage 24', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1472', '48', 'Storage 25', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1473', '48', 'Storage 26', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1474', '48', 'Storage 27', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1475', '48', 'Storage 28', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1476', '48', 'Storage 29', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1477', '48', 'Storage 30', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1478', '48', 'Storage 31', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1479', '48', 'Storage 32', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1480', '48', 'Storage 33', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1481', '48', 'Storage 34', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1482', '48', 'Storage 35', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1483', '48', 'Storage 36', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1484', '48', 'Storage 37', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1485', '48', 'Storage 38', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1486', '48', 'Storage 39', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1487', '48', 'Storage 40', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1488', '48', 'Storage 41', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1489', '48', 'Storage 42', 'slot', null, '2013-12-01 17:21:58', '1');
INSERT INTO `core_storage` VALUES ('1490', '49', 'Storage 1', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1491', '49', 'Storage 2', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1492', '49', 'Storage 3', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1493', '49', 'Storage 4', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1494', '49', 'Storage 5', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1495', '49', 'Storage 6', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1496', '49', 'Storage 7', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1497', '49', 'Storage 8', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1498', '49', 'Storage 9', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1499', '49', 'Storage 10', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1500', '49', 'Storage 11', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1501', '49', 'Storage 12', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1502', '49', 'Storage 13', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1503', '49', 'Storage 14', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1504', '49', 'Storage 15', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1505', '49', 'Storage 16', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1506', '49', 'Storage 17', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1507', '49', 'Storage 18', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1508', '49', 'Storage 19', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1509', '49', 'Storage 20', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1510', '49', 'Storage 21', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1511', '49', 'Storage 22', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1512', '49', 'Storage 23', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1513', '49', 'Storage 24', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1514', '49', 'Storage 25', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1515', '49', 'Storage 26', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1516', '49', 'Storage 27', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1517', '49', 'Storage 28', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1518', '49', 'Storage 29', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1519', '49', 'Storage 30', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1520', '49', 'Storage 31', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1521', '49', 'Storage 32', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1522', '49', 'Storage 33', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1523', '49', 'Storage 34', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1524', '49', 'Storage 35', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1525', '49', 'Storage 36', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1526', '49', 'Storage 37', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1527', '49', 'Storage 38', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1528', '49', 'Storage 39', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1529', '49', 'Storage 40', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1530', '49', 'Storage 41', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1531', '49', 'Storage 42', 'slot', null, '2013-12-01 17:22:02', '1');
INSERT INTO `core_storage` VALUES ('1532', '50', 'Storage 1', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1533', '50', 'Storage 2', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1534', '50', 'Storage 3', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1535', '50', 'Storage 4', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1536', '50', 'Storage 5', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1537', '50', 'Storage 6', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1538', '50', 'Storage 7', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1539', '50', 'Storage 8', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1540', '50', 'Storage 9', 'slot', null, '2013-12-01 17:23:14', '1');
INSERT INTO `core_storage` VALUES ('1541', '50', 'Storage 10', 'slot', null, '2013-12-01 17:23:14', '1');

-- ----------------------------
-- Table structure for core_test
-- ----------------------------
DROP TABLE IF EXISTS `core_test`;
CREATE TABLE `core_test` (
  `test_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`test_id`),
  UNIQUE KEY `ndx_name` (`name`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_test
-- ----------------------------
INSERT INTO `core_test` VALUES ('1', 'HTRB', null, null, '2013-11-28 14:24:23', '1');
INSERT INTO `core_test` VALUES ('2', 'HTGB', null, null, '2013-11-28 14:24:28', '1');
INSERT INTO `core_test` VALUES ('3', 'HAST', null, null, '2013-11-28 14:24:31', '1');
INSERT INTO `core_test` VALUES ('4', 'TMCL', null, null, '2013-11-28 14:24:34', '1');
INSERT INTO `core_test` VALUES ('5', 'PRECON', null, null, '2013-11-28 14:24:39', '1');
INSERT INTO `core_test` VALUES ('6', 'PTMCL', null, null, '2013-11-28 14:24:45', '1');
INSERT INTO `core_test` VALUES ('7', 'TMSK', null, null, '2013-11-28 14:25:02', '1');
INSERT INTO `core_test` VALUES ('8', 'PRCL (Bipolar)', null, null, '2013-11-28 14:25:10', '1');
INSERT INTO `core_test` VALUES ('9', 'PRCL (DMOS)', null, null, '2013-11-28 14:25:16', '1');
INSERT INTO `core_test` VALUES ('10', 'DOPL/SOPL/HTOL', null, null, '2013-11-28 14:25:25', '1');
INSERT INTO `core_test` VALUES ('11', 'ACLV', null, null, '2013-11-28 14:25:29', '1');
INSERT INTO `core_test` VALUES ('12', 'THBT/H3TRB', null, null, '2013-11-28 14:25:42', '1');
INSERT INTO `core_test` VALUES ('13', 'ZOPL', null, null, '2013-11-28 14:25:50', '1');

-- ----------------------------
-- Table structure for core_test_condition
-- ----------------------------
DROP TABLE IF EXISTS `core_test_condition`;
CREATE TABLE `core_test_condition` (
  `test_condition_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` bigint(20) unsigned NOT NULL,
  `condition_id` bigint(20) unsigned NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`test_condition_id`),
  UNIQUE KEY `ndx_test_condition_id` (`test_id`,`condition_id`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE,
  KEY `ndx_test_id` (`test_id`) USING BTREE,
  KEY `ndx_condition_id` (`condition_id`) USING BTREE,
  CONSTRAINT `core_test_condition_ibfk_1` FOREIGN KEY (`condition_id`) REFERENCES `core_condition` (`condition_id`),
  CONSTRAINT `core_test_condition_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `core_test` (`test_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_test_condition
-- ----------------------------
INSERT INTO `core_test_condition` VALUES ('1', '1', '1', '2013-11-28 15:37:08', '1');
INSERT INTO `core_test_condition` VALUES ('2', '1', '3', '2013-11-28 15:37:46', '1');
INSERT INTO `core_test_condition` VALUES ('3', '1', '4', '2013-11-28 15:38:05', '1');
INSERT INTO `core_test_condition` VALUES ('4', '2', '3', '2013-11-28 15:38:57', '1');
INSERT INTO `core_test_condition` VALUES ('5', '2', '4', '2013-11-28 15:39:03', '1');
INSERT INTO `core_test_condition` VALUES ('6', '3', '7', '2013-11-28 15:44:22', '1');
INSERT INTO `core_test_condition` VALUES ('7', '3', '8', '2013-11-28 15:44:27', '1');
INSERT INTO `core_test_condition` VALUES ('8', '4', '9', '2013-11-28 15:45:54', '1');
INSERT INTO `core_test_condition` VALUES ('9', '4', '10', '2013-11-28 15:45:58', '1');
INSERT INTO `core_test_condition` VALUES ('10', '5', '11', '2013-11-28 15:46:20', '1');
INSERT INTO `core_test_condition` VALUES ('11', '5', '12', '2013-11-28 15:46:26', '1');
INSERT INTO `core_test_condition` VALUES ('12', '6', '13', '2013-11-28 15:48:15', '1');
INSERT INTO `core_test_condition` VALUES ('13', '7', '14', '2013-11-28 15:48:50', '1');
INSERT INTO `core_test_condition` VALUES ('14', '8', '15', '2013-11-28 15:49:57', '1');
INSERT INTO `core_test_condition` VALUES ('15', '9', '16', '2013-11-28 15:50:33', '1');
INSERT INTO `core_test_condition` VALUES ('16', '10', '17', '2013-11-28 15:51:12', '1');
INSERT INTO `core_test_condition` VALUES ('17', '11', '18', '2013-11-28 15:51:39', '1');
INSERT INTO `core_test_condition` VALUES ('18', '12', '19', '2013-11-28 15:51:44', '1');
INSERT INTO `core_test_condition` VALUES ('19', '13', '20', '2013-11-28 15:51:51', '1');

-- ----------------------------
-- Table structure for core_user
-- ----------------------------
DROP TABLE IF EXISTS `core_user`;
CREATE TABLE `core_user` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `position_id` bigint(20) unsigned DEFAULT NULL,
  `email_address` varchar(150) NOT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `password` varchar(255) NOT NULL DEFAULT '81DC9BDB52D04DC20036DBD8313ED055' COMMENT 'MD5 encryption default 1234',
  `last_updated` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = no, 1 = yes',
  `status_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = inactive, 1 = active',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ndx_email_address` (`email_address`) USING BTREE,
  KEY `ndx_department_id` (`department_id`) USING BTREE,
  KEY `ndx_position_id` (`position_id`) USING BTREE,
  KEY `ndx_status_id` (`status_id`) USING BTREE,
  CONSTRAINT `fk_cus_department_id` FOREIGN KEY (`department_id`) REFERENCES `core_department` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_user
-- ----------------------------
